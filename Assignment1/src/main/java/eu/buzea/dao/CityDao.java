package eu.buzea.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import eu.buzea.model.City;
import eu.buzea.util.HibernateUtil;

public class CityDao {

	public CityDao() {
	}

	@SuppressWarnings("unchecked")
	public List<City> findAll() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		List<City> list = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM City");
			list = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public City find(int id) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		City city = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM City WHERE idcity = :id");
			query.setParameter("id", id);
			List<City> list = query.list();
			tx.commit();
			if (list != null && list.size() > 0) {
				city = list.get(0);
			}
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return city;
	}

}
