package eu.buzea.server.services;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import eu.buzea.entities.Car;
import eu.buzea.remotes.TaxServiceRemote;

public class TaxServiceImpl extends UnicastRemoteObject implements TaxServiceRemote {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3414927439964792615L;

	public TaxServiceImpl() throws RemoteException {
	}

	@Override
	public double calculateTax(Car c) throws RemoteException {
		if (c.getEngineCapacity() <= 0) {
			throw new IllegalArgumentException("Engine capacity must be positive.");
		}
		int sum = 8;
		if (c.getEngineCapacity() > 1601)
			sum = 18;
		if (c.getEngineCapacity() > 2001)
			sum = 72;
		if (c.getEngineCapacity() > 2601)
			sum = 144;
		if (c.getEngineCapacity() > 3001)
			sum = 290;
		return c.getEngineCapacity() / 200.0 * sum;
	}

}
