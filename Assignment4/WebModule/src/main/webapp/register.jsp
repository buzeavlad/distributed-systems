<?xml version="1.0" encoding="ISO-8859-1" ?>
<jsp:root xmlns:jsp="http://java.sun.com/JSP/Page" version="2.0">
	<jsp:directive.page contentType="text/html; charset=ISO-8859-1"
		pageEncoding="ISO-8859-1" session="false" />
	<jsp:output doctype-root-element="html"
		doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
		doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
		omit-xml-declaration="true" />
	<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" href="css/bootstrap.css" />
<link rel="stylesheet" href="css/bootstrap-theme.css" />
<title>Tracking System</title>
<SCRIPT type="text/javascript">
	window.onload = function() {
		document.getElementById("mySubmit").disabled = true;
	}

	function validate() {
		var p1 = document.getElementById("p1").value;
		var p2 = document.getElementById("p2").value;
		var username = document.getElementById("username").value;
		var notRobot = document.getElementById("robot").checked;

		var len = (3 > p1.length);
		var uLen = (3 > username.length);
		if (p1 != p2) {
			alert("Passwords do not match");
			document.getElementById("p1").focus();
			document.getElementById("robot").checked = false;
		} else {
			if (len) {
				alert("Passwords must have at least 3 characters");
				document.getElementById("p1").focus();
				document.getElementById("robot").checked = false;
			} else {
				if (uLen) {
					alert("Username must have at least 3 characters");
					document.getElementById("username").focus();
					document.getElementById("robot").checked = false;
				} else {
					if (notRobot) {
						document.getElementById("mySubmit").disabled = false;
					}else{
						document.getElementById("mySubmit").disabled = true;
					}
				}
			}
		}
	}
</SCRIPT>
</head>
<body>
	<div align="center">
		<h1>Client Registration</h1>
		<FORM action="logIn">
			<table>
				<tr>
					<td>Username:</td>
					<td><INPUT id="username" name="username" type="text" /></td>
				</tr>
				<tr>
					<td>Password:</td>
					<td><INPUT id="p1" name="password" type="password" /></td>
				</tr>
				<tr>
					<td>Repeat Password:</td>
					<td><INPUT id="p2" name="password2" type="password" /></td>
				</tr>
				<tr>
					<TD><INPUT type="checkbox" name="robot" id="robot"
						onclick="validate()" />I am not a Robot</TD>
				</tr>
				<tr>
					<td><INPUT id="mySubmit" type="submit" name="register"
						value="Register" /></td>
					<td><INPUT type="button" value="Cancel"
						onclick="window.location='.'" /></td>
				</tr>
			</table>
		</FORM>
	</div>
</body>
	</html>
</jsp:root>