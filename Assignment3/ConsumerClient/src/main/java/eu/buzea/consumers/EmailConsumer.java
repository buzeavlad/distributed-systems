package eu.buzea.consumers;

import java.io.IOException;

import org.apache.commons.lang.SerializationUtils;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

import eu.buzea.dvd.Dvd;
import eu.buzea.services.MailService;

public class EmailConsumer extends DefaultConsumer {

	private MailService mailService;
	// private TextService textService;

	public EmailConsumer(Channel channel) {
		super(channel);
		mailService = new MailService("dsUser.buzea@gmail.com", "Software");
		mailService.addSubscriberEmail("buzea.vlad@gmail.com");
		mailService.addSubscriberEmail("vlad_by_2005@yahoo.com");
		// textService = new TextService();
	}

	@Override
	public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
		Dvd dvd = (Dvd) SerializationUtils.deserialize(body);
		if (dvd != null) {
			System.out.println("Processing: " + dvd);
			String dvdString = "DVD:\n\t" + "Artist: " + dvd.getArtist() + "\n\tAlbum: " + dvd.getAlbumName() + "\n\tPrice: " + dvd.getPrice() + "$\n\tYear: " + dvd.getYear() + "\n";
			mailService.sendToAllSubscribers("A new DVD has been added", dvdString + "\n\nWe wish you a nice day!");
			// textService.writeDvdToFile(dvd);
		}
	}

}
