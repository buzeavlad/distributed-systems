<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Local time</title>
<link rel="stylesheet" href="css/bootstrap.css" />
<link rel="stylesheet" href="css/bootstrap-theme.css" />
</head>
<body>
	<center>
		<h1>Local time for departure and arrival cities</h1>
		<table border="2px">
			<tr>
				<th>Local time of departure</th>
				<th>Local time of arrival</th>
			</tr>
			<tr>
				<td>${depart}</td>
				<td>${arrival}</td>
			</tr>
		</table>
	</center>

</body>
</html>