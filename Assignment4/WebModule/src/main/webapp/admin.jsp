<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="stylesheet" href="css/bootstrap.css" />
<link rel="stylesheet" href="css/bootstrap-theme.css" />
<title>Tracking system</title>
<script>
	function goToAddPackage() {

	}
</script>
</head>
<body>

	<%
		boolean authorized = false;
		Object admin = request.getSession().getAttribute("admin");
		if (admin != null) {
			authorized = true;
		}

		if (!authorized) {
			response.sendRedirect("index.jsp");
		}
	%>
	<div id="myHeader" align="right">
		<table>
			<tr>
				<td>
					<form action="logIn">
						<input name="logOut" value="Log Out" type="submit" />
					</form>
				</td>
			</tr>
			<tr>
				<td>
					<form action="AdminActions">
						<input type="submit" name="add" value="Add new package" />
					</form>
				</td>
			</tr>
		</table>

	</div>
	<div align="center">
		<H1>Admin Command</H1>
		<table border="1">
			<tr>
				<TH colspan="9">Package list</TH>
			</tr>
			<tr>
				<TH>Name</TH>
				<TH>Description</TH>
				<TH>Sender</TH>
				<TH>Receiver</TH>
				<TH>From</TH>
				<TH>To</TH>
				<TH>Status</TH>
				<TH>Tracking</TH>
				<TH>Actions</TH>
			</tr>
			<c:forEach var="pkg" items="${packageList}">
				<tr>
					<td>${pkg.name}</td>
					<td>${pkg.description}</td>
					<td>${pkg.sender.username}</td>
					<td>${pkg.receiver.username}</td>
					<td>${pkg.senderCity.name}</td>
					<td>${pkg.destinationCity.name}</td>
					<td>${pkg.status}</td>
					<td>${pkg.tracking}</td>
					<td><FORM action="./AdminActions" method="post">
							<INPUT type="submit" name="delete" value="Delete Package" /> <input
								type="hidden" value="${pkg.name}" name="pkgName" />
							<c:choose>
								<c:when test="${pkg.tracking ==true}">
									<INPUT type="submit" value="Add Route Entry" name="add" />
								</c:when>
								<c:otherwise>
									<input type="submit" value="Enable tracking" name="enable" />

								</c:otherwise>
							</c:choose>
						</FORM></td>
				</tr>

			</c:forEach>
		</table>
	</div>

</body>
</html>
