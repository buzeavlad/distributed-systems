
package eu.buzea;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * Client WebService
 * 
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "ClientWs", targetNamespace = "http://buzea.eu/", wsdlLocation = "http://localhost:49578/ClientWs.asmx?WSDL")
public class ClientWs
    extends Service
{

    private final static URL CLIENTWS_WSDL_LOCATION;
    private final static WebServiceException CLIENTWS_EXCEPTION;
    private final static QName CLIENTWS_QNAME = new QName("http://buzea.eu/", "ClientWs");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("http://localhost:49578/ClientWs.asmx?WSDL");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        CLIENTWS_WSDL_LOCATION = url;
        CLIENTWS_EXCEPTION = e;
    }

    public ClientWs() {
        super(__getWsdlLocation(), CLIENTWS_QNAME);
    }

    public ClientWs(WebServiceFeature... features) {
        super(__getWsdlLocation(), CLIENTWS_QNAME, features);
    }

    public ClientWs(URL wsdlLocation) {
        super(wsdlLocation, CLIENTWS_QNAME);
    }

    public ClientWs(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, CLIENTWS_QNAME, features);
    }

    public ClientWs(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public ClientWs(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns ClientWsSoap
     */
    @WebEndpoint(name = "ClientWsSoap")
    public ClientWsSoap getClientWsSoap() {
        return super.getPort(new QName("http://buzea.eu/", "ClientWsSoap"), ClientWsSoap.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns ClientWsSoap
     */
    @WebEndpoint(name = "ClientWsSoap")
    public ClientWsSoap getClientWsSoap(WebServiceFeature... features) {
        return super.getPort(new QName("http://buzea.eu/", "ClientWsSoap"), ClientWsSoap.class, features);
    }

    private static URL __getWsdlLocation() {
        if (CLIENTWS_EXCEPTION!= null) {
            throw CLIENTWS_EXCEPTION;
        }
        return CLIENTWS_WSDL_LOCATION;
    }

}
