
package eu.buzea.ws.admin;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the eu.buzea.ws.admin package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _FindAllPackagesResponse_QNAME = new QName("http://admin.ws.buzea.eu/", "findAllPackagesResponse");
    private final static QName _FindByNameResponse_QNAME = new QName("http://admin.ws.buzea.eu/", "findByNameResponse");
    private final static QName _AddRouteEntryResponse_QNAME = new QName("http://admin.ws.buzea.eu/", "addRouteEntryResponse");
    private final static QName _SetDeliveredResponse_QNAME = new QName("http://admin.ws.buzea.eu/", "setDeliveredResponse");
    private final static QName _CreatePackage_QNAME = new QName("http://admin.ws.buzea.eu/", "createPackage");
    private final static QName _DeletePackageResponse_QNAME = new QName("http://admin.ws.buzea.eu/", "deletePackageResponse");
    private final static QName _AddRouteEntry_QNAME = new QName("http://admin.ws.buzea.eu/", "addRouteEntry");
    private final static QName _CreatePackageResponse_QNAME = new QName("http://admin.ws.buzea.eu/", "createPackageResponse");
    private final static QName _EnableTracking_QNAME = new QName("http://admin.ws.buzea.eu/", "enableTracking");
    private final static QName _EnableTrackingResponse_QNAME = new QName("http://admin.ws.buzea.eu/", "enableTrackingResponse");
    private final static QName _FindAllCitiesResponse_QNAME = new QName("http://admin.ws.buzea.eu/", "findAllCitiesResponse");
    private final static QName _SetDelivered_QNAME = new QName("http://admin.ws.buzea.eu/", "setDelivered");
    private final static QName _DeletePackage_QNAME = new QName("http://admin.ws.buzea.eu/", "deletePackage");
    private final static QName _GetRoute_QNAME = new QName("http://admin.ws.buzea.eu/", "getRoute");
    private final static QName _FindAllCities_QNAME = new QName("http://admin.ws.buzea.eu/", "findAllCities");
    private final static QName _FindAllPackages_QNAME = new QName("http://admin.ws.buzea.eu/", "findAllPackages");
    private final static QName _FindByName_QNAME = new QName("http://admin.ws.buzea.eu/", "findByName");
    private final static QName _GetRouteResponse_QNAME = new QName("http://admin.ws.buzea.eu/", "getRouteResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: eu.buzea.ws.admin
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AddRouteEntry }
     * 
     */
    public AddRouteEntry createAddRouteEntry() {
        return new AddRouteEntry();
    }

    /**
     * Create an instance of {@link CreatePackageResponse }
     * 
     */
    public CreatePackageResponse createCreatePackageResponse() {
        return new CreatePackageResponse();
    }

    /**
     * Create an instance of {@link EnableTracking }
     * 
     */
    public EnableTracking createEnableTracking() {
        return new EnableTracking();
    }

    /**
     * Create an instance of {@link DeletePackageResponse }
     * 
     */
    public DeletePackageResponse createDeletePackageResponse() {
        return new DeletePackageResponse();
    }

    /**
     * Create an instance of {@link CreatePackage }
     * 
     */
    public CreatePackage createCreatePackage() {
        return new CreatePackage();
    }

    /**
     * Create an instance of {@link FindAllCities }
     * 
     */
    public FindAllCities createFindAllCities() {
        return new FindAllCities();
    }

    /**
     * Create an instance of {@link FindAllPackages }
     * 
     */
    public FindAllPackages createFindAllPackages() {
        return new FindAllPackages();
    }

    /**
     * Create an instance of {@link FindByName }
     * 
     */
    public FindByName createFindByName() {
        return new FindByName();
    }

    /**
     * Create an instance of {@link GetRouteResponse }
     * 
     */
    public GetRouteResponse createGetRouteResponse() {
        return new GetRouteResponse();
    }

    /**
     * Create an instance of {@link DeletePackage }
     * 
     */
    public DeletePackage createDeletePackage() {
        return new DeletePackage();
    }

    /**
     * Create an instance of {@link GetRoute }
     * 
     */
    public GetRoute createGetRoute() {
        return new GetRoute();
    }

    /**
     * Create an instance of {@link FindAllCitiesResponse }
     * 
     */
    public FindAllCitiesResponse createFindAllCitiesResponse() {
        return new FindAllCitiesResponse();
    }

    /**
     * Create an instance of {@link SetDelivered }
     * 
     */
    public SetDelivered createSetDelivered() {
        return new SetDelivered();
    }

    /**
     * Create an instance of {@link EnableTrackingResponse }
     * 
     */
    public EnableTrackingResponse createEnableTrackingResponse() {
        return new EnableTrackingResponse();
    }

    /**
     * Create an instance of {@link SetDeliveredResponse }
     * 
     */
    public SetDeliveredResponse createSetDeliveredResponse() {
        return new SetDeliveredResponse();
    }

    /**
     * Create an instance of {@link AddRouteEntryResponse }
     * 
     */
    public AddRouteEntryResponse createAddRouteEntryResponse() {
        return new AddRouteEntryResponse();
    }

    /**
     * Create an instance of {@link FindAllPackagesResponse }
     * 
     */
    public FindAllPackagesResponse createFindAllPackagesResponse() {
        return new FindAllPackagesResponse();
    }

    /**
     * Create an instance of {@link FindByNameResponse }
     * 
     */
    public FindByNameResponse createFindByNameResponse() {
        return new FindByNameResponse();
    }

    /**
     * Create an instance of {@link City }
     * 
     */
    public City createCity() {
        return new City();
    }

    /**
     * Create an instance of {@link PackageDto }
     * 
     */
    public PackageDto createPackageDto() {
        return new PackageDto();
    }

    /**
     * Create an instance of {@link PackageItem }
     * 
     */
    public PackageItem createPackageItem() {
        return new PackageItem();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link RouteEntry }
     * 
     */
    public RouteEntry createRouteEntry() {
        return new RouteEntry();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllPackagesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://admin.ws.buzea.eu/", name = "findAllPackagesResponse")
    public JAXBElement<FindAllPackagesResponse> createFindAllPackagesResponse(FindAllPackagesResponse value) {
        return new JAXBElement<FindAllPackagesResponse>(_FindAllPackagesResponse_QNAME, FindAllPackagesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindByNameResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://admin.ws.buzea.eu/", name = "findByNameResponse")
    public JAXBElement<FindByNameResponse> createFindByNameResponse(FindByNameResponse value) {
        return new JAXBElement<FindByNameResponse>(_FindByNameResponse_QNAME, FindByNameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddRouteEntryResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://admin.ws.buzea.eu/", name = "addRouteEntryResponse")
    public JAXBElement<AddRouteEntryResponse> createAddRouteEntryResponse(AddRouteEntryResponse value) {
        return new JAXBElement<AddRouteEntryResponse>(_AddRouteEntryResponse_QNAME, AddRouteEntryResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetDeliveredResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://admin.ws.buzea.eu/", name = "setDeliveredResponse")
    public JAXBElement<SetDeliveredResponse> createSetDeliveredResponse(SetDeliveredResponse value) {
        return new JAXBElement<SetDeliveredResponse>(_SetDeliveredResponse_QNAME, SetDeliveredResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreatePackage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://admin.ws.buzea.eu/", name = "createPackage")
    public JAXBElement<CreatePackage> createCreatePackage(CreatePackage value) {
        return new JAXBElement<CreatePackage>(_CreatePackage_QNAME, CreatePackage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeletePackageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://admin.ws.buzea.eu/", name = "deletePackageResponse")
    public JAXBElement<DeletePackageResponse> createDeletePackageResponse(DeletePackageResponse value) {
        return new JAXBElement<DeletePackageResponse>(_DeletePackageResponse_QNAME, DeletePackageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AddRouteEntry }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://admin.ws.buzea.eu/", name = "addRouteEntry")
    public JAXBElement<AddRouteEntry> createAddRouteEntry(AddRouteEntry value) {
        return new JAXBElement<AddRouteEntry>(_AddRouteEntry_QNAME, AddRouteEntry.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreatePackageResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://admin.ws.buzea.eu/", name = "createPackageResponse")
    public JAXBElement<CreatePackageResponse> createCreatePackageResponse(CreatePackageResponse value) {
        return new JAXBElement<CreatePackageResponse>(_CreatePackageResponse_QNAME, CreatePackageResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnableTracking }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://admin.ws.buzea.eu/", name = "enableTracking")
    public JAXBElement<EnableTracking> createEnableTracking(EnableTracking value) {
        return new JAXBElement<EnableTracking>(_EnableTracking_QNAME, EnableTracking.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnableTrackingResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://admin.ws.buzea.eu/", name = "enableTrackingResponse")
    public JAXBElement<EnableTrackingResponse> createEnableTrackingResponse(EnableTrackingResponse value) {
        return new JAXBElement<EnableTrackingResponse>(_EnableTrackingResponse_QNAME, EnableTrackingResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllCitiesResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://admin.ws.buzea.eu/", name = "findAllCitiesResponse")
    public JAXBElement<FindAllCitiesResponse> createFindAllCitiesResponse(FindAllCitiesResponse value) {
        return new JAXBElement<FindAllCitiesResponse>(_FindAllCitiesResponse_QNAME, FindAllCitiesResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetDelivered }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://admin.ws.buzea.eu/", name = "setDelivered")
    public JAXBElement<SetDelivered> createSetDelivered(SetDelivered value) {
        return new JAXBElement<SetDelivered>(_SetDelivered_QNAME, SetDelivered.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeletePackage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://admin.ws.buzea.eu/", name = "deletePackage")
    public JAXBElement<DeletePackage> createDeletePackage(DeletePackage value) {
        return new JAXBElement<DeletePackage>(_DeletePackage_QNAME, DeletePackage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRoute }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://admin.ws.buzea.eu/", name = "getRoute")
    public JAXBElement<GetRoute> createGetRoute(GetRoute value) {
        return new JAXBElement<GetRoute>(_GetRoute_QNAME, GetRoute.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllCities }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://admin.ws.buzea.eu/", name = "findAllCities")
    public JAXBElement<FindAllCities> createFindAllCities(FindAllCities value) {
        return new JAXBElement<FindAllCities>(_FindAllCities_QNAME, FindAllCities.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindAllPackages }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://admin.ws.buzea.eu/", name = "findAllPackages")
    public JAXBElement<FindAllPackages> createFindAllPackages(FindAllPackages value) {
        return new JAXBElement<FindAllPackages>(_FindAllPackages_QNAME, FindAllPackages.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindByName }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://admin.ws.buzea.eu/", name = "findByName")
    public JAXBElement<FindByName> createFindByName(FindByName value) {
        return new JAXBElement<FindByName>(_FindByName_QNAME, FindByName.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetRouteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://admin.ws.buzea.eu/", name = "getRouteResponse")
    public JAXBElement<GetRouteResponse> createGetRouteResponse(GetRouteResponse value) {
        return new JAXBElement<GetRouteResponse>(_GetRouteResponse_QNAME, GetRouteResponse.class, null, value);
    }

}
