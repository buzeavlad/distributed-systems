package eu.buzea.dto;

public class PackageDto {

	private String senderUsername, receiverUsername, packageName, description, senderCityName, destinationCityName;

	public String getSenderCityName() {
		return senderCityName;
	}

	public void setSenderCityName(String senderCityName) {
		this.senderCityName = senderCityName;
	}

	public String getDestinationCityName() {
		return destinationCityName;
	}

	public void setDestinationCityName(String destinationCityName) {
		this.destinationCityName = destinationCityName;
	}

	public PackageDto(String senderUsername, String receiverUsername, String packageName, String description, String senderCityName, String destinationCityName) {
		super();
		this.senderUsername = senderUsername;
		this.receiverUsername = receiverUsername;
		this.packageName = packageName;
		this.description = description;
		this.senderCityName = senderCityName;
		this.destinationCityName = destinationCityName;
	}

	public PackageDto() {

	}

	public String getSenderUsername() {
		return senderUsername;
	}

	public void setSenderUsername(String senderUsername) {
		this.senderUsername = senderUsername;
	}

	public String getReceiverUsername() {
		return receiverUsername;
	}

	public void setReceiverUsername(String receiverUsername) {
		this.receiverUsername = receiverUsername;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
