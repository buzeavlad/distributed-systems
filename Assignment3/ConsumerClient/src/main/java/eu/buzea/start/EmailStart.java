package eu.buzea.start;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import eu.buzea.consumers.EmailConsumer;
import eu.buzea.messaging.RabbitMQEndPoint;

public class EmailStart extends RabbitMQEndPoint {

	public EmailStart(String queueName) throws IOException, TimeoutException {
		super(queueName);
	}

	public void receiveDvds() {
		EmailConsumer consumer = new EmailConsumer(channel);
		while(true){
			try {
				channel.basicConsume(queueName, true, consumer);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		
	}
	
	public static void main(String[] args) throws IOException, TimeoutException {
		EmailStart elem = new EmailStart("emailQueue");
		elem.receiveDvds();
	}
}
