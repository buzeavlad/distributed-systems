package eu.buzea.messaging;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Queue;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public abstract class JmsEndPoint {

	private static final String JMS_MY_CONNECTION_FACTORY = "jms/MyConnectionFactory";
	protected Queue myQueue;
	protected Destination destination;
	protected Session mySess;
	protected Connection myConn;

	public JmsEndPoint(String queueName) {
		ConnectionFactory myConnFactory ;
		try {

	
			Context ctx = null;
			/*
			 * Hashtable<String,String> env;
			 * 
			 * 
			 * env = new Hashtable<String, String>();
			 * 
			 * 
			 * env.put(Context.INITIAL_CONTEXT_FACTORY,
			 * "com.sun.jndi.fscontext.RefFSContextFactory"); // On Unix, use
			 * file:///tmp instead of file:///C:/Temp
			 * env.put(Context.PROVIDER_URL, "file:///C:/Temp");
			 */

			// Create the initial context.
			ctx = new InitialContext();

			// Lookup my connection factory from the admin object store.
			// The name used here here must match the lookup name
			// used when the admin object was stored.
			myConnFactory = (javax.jms.QueueConnectionFactory) ctx.lookup(JMS_MY_CONNECTION_FACTORY);

			myQueue = (Queue) ctx.lookup(queueName);
			myConn = myConnFactory.createConnection();
			mySess = myConn.createSession(false, Session.AUTO_ACKNOWLEDGE);
			// myQueue = new com.sun.messaging.Queue(queueName);
			myConn.start();
		} catch (JMSException | NamingException e) {
			e.printStackTrace();
		}

	}

	public void closeAll() {
		try {
			mySess.close();
		} catch (JMSException | NullPointerException e) {
			mySess = null;
		}
		try {
			myConn.close();
		} catch (JMSException | NullPointerException e) {
			myConn = null;
		}
	}

}
