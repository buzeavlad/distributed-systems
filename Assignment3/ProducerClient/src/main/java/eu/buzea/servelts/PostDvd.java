package eu.buzea.servelts;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.TimeoutException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import eu.buzea.dvd.Dvd;
import eu.buzea.producer.JmsProducer;
import eu.buzea.producer.RabbitMqProducer;

/**
 * Servlet implementation class PostDvd
 */
@WebServlet({ "/PostDvd", "/postDvd" })
public class PostDvd extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PostDvd() {
		super();

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String artist = request.getParameter("artist");
		String albumName = request.getParameter("album");
		int year = Integer.parseInt(request.getParameter("year"));
		double price = Double.parseDouble(request.getParameter("price"));
		String message= "ERROR";
		if ("".equals(artist) || "".equals(albumName)) {
			message = "All DVDs must have an artist and an Album Name";
		} else {
			RabbitMqProducer emailProducer;
			try {
				emailProducer = new RabbitMqProducer("emailQueue");
			
			JmsProducer textProducer = new JmsProducer("jms/textQueue");
			Dvd dvd = new Dvd(artist, albumName, price, year);
			boolean sent = emailProducer.sendDvd(dvd) & 
					textProducer.sendDvd(dvd);
			if (sent) {
				message = "DVD added";
			} else {
				message = "Error sending message to server";
			}
			emailProducer.closeAll();
			textProducer.closeAll();
			} catch (TimeoutException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		respond(response, message);
	}

	private void respond(HttpServletResponse response, String message) throws IOException {
		PrintWriter out = response.getWriter();
		out.println("<script type=\"text/javascript\">");
		out.println("alert('" + message + "');");
		out.println("location='index.jsp';");
		out.println("</script>");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
