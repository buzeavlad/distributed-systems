package ro.tuc.dsrl.ds.handson.assig.two.client.communication;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.ISellingService;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.ITaxService;
import ro.tuc.dsrl.ds.handson.assig.two.rpc.Naming;

import java.io.IOException;

/**
 * @Author: Technical University of Cluj-Napoca, Romania Distributed Systems,
 *          http://dsrl.coned.utcluj.ro/
 * @Module: assignment-two-client
 * @Since: Sep 24, 2015
 * @Description: Starting point of the Client application.
 */
public class ClientStart {
	private static final Log LOGGER = LogFactory.getLog(ClientStart.class);

	private ClientStart() {
	}

	public static void main(String[] args) {
		ITaxService taxService = null;
		ISellingService sellingService = null;
		try {
			taxService = Naming.lookup(ITaxService.class, ServerConnection.getInstance());

			System.out.println("Tax value: " + taxService.computeTax(new Car(2009, 2000)));
			// System.out.println(taxService.computeTax(new Car(2009, -1)));

			// ServerConnection.getInstance().closeAll();
		} catch (IOException e) {
			LOGGER.error("", e);
		}

		try {
			Car car = new Car(2014, 2000, 10000);
			sellingService = Naming.lookup(ISellingService.class, ServerConnection.getInstance());
			System.out.println("Selling price: " + sellingService.calculateSellingPrice(car));
		} catch (IOException e) {
			LOGGER.error("", e);
		}finally {
			try {
				ServerConnection.getInstance().closeAll();
			} catch (IOException e) {
				
			}
		}
		
	}
}
