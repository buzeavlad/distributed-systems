package eu.buzea.servlets;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import eu.buzea.dao.AirplaneDao;
import eu.buzea.dao.BookingDao;
import eu.buzea.dao.CityDao;
import eu.buzea.dao.FlightDao;
import eu.buzea.dao.UserDao;
import eu.buzea.model.Airplane;
import eu.buzea.model.Booking;
import eu.buzea.model.City;
import eu.buzea.model.Flight;
import eu.buzea.model.User;

/**
 * Servlet implementation class AdminServlet
 */
@WebServlet("/AdminServlet")
public class AdminServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	private AirplaneDao airplaneDao;
	private CityDao cityDao;
	private UserDao userDao;
	private BookingDao bookingDao;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdminServlet() {
		super();

	}

	@Override
	public void init() throws ServletException {
		super.init();
		airplaneDao = new AirplaneDao();
		cityDao = new CityDao();
		userDao = new UserDao();
		bookingDao = new BookingDao();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String buttonPressed = request.getParameter("buttonPressed");
		request.setAttribute("buttonPressed", buttonPressed);
		FlightDao flightDao = new FlightDao();
		if (buttonPressed == null) {
			response.sendRedirect("index.jsp");
		}

		List<Airplane> airplanes;
		List<City> cities;
		List<Flight> flights;
		String flightNbString = request.getParameter("flightNumber");
		int flightNb = 0;
		Flight flight = null;
		if (flightNbString != null) {
			flightNb = Integer.parseInt(flightNbString);
			flight = flightDao.find(flightNb);
		}

		switch (buttonPressed) {
		case "Delete flight":

			flightDao.delete(flightNb);

			flights = flightDao.findAll();
			request.setAttribute("flights", flights);
			request.getRequestDispatcher("admin.jsp").forward(request, response);
			break;

		case "Update flight":

			request.setAttribute("flight", flight);

			String arrivalTime = DATE_FORMAT.format(flight.getArrivalTime());
			arrivalTime = arrivalTime.replace(" ", "T");
			request.setAttribute("arrivalTime", arrivalTime);

			String departTime = DATE_FORMAT.format(flight.getDepartureTime());
			departTime = departTime.replace(" ", "T");
			request.setAttribute("departTime", departTime);

			airplanes = airplaneDao.findAll();
			request.setAttribute("airplanes", airplanes);
			cities = cityDao.findAll();
			request.setAttribute("cities", cities);
			request.getRequestDispatcher("flight.jsp").forward(request, response);
			break;

		case "Create new flight":
			airplanes = airplaneDao.findAll();
			request.setAttribute("airplanes", airplanes);
			cities = cityDao.findAll();
			request.setAttribute("cities", cities);

			Date now = new Date();
			Calendar cal = Calendar.getInstance(); // creates calendar
			cal.setTime(now); // sets calendar time/date
			cal.add(Calendar.HOUR_OF_DAY, 1); // adds one hour
			Date future = cal.getTime();

			String defaultArrivalTime = DATE_FORMAT.format(future);
			defaultArrivalTime = defaultArrivalTime.replace(" ", "T");
			request.setAttribute("arrivalTime", defaultArrivalTime);

			String defaultDepartTime = DATE_FORMAT.format(now);
			defaultDepartTime = defaultDepartTime.replace(" ", "T");
			request.setAttribute("departTime", defaultDepartTime);

			request.getRequestDispatcher("flight.jsp").forward(request, response);
			break;
		case "Create new booking":
			String username = request.getParameter("username");
			User user = userDao.findUser(username);
			if (user != null) {
				Booking booking = new Booking(flight, user);
				bookingDao.create(booking);
				request.setAttribute("message", "Booking created!");
			} else {
				request.setAttribute("message", "Invalid username!");
			}

			flights = flightDao.findAll();
			request.setAttribute("flights", flights);
			request.getRequestDispatcher("admin.jsp").forward(request, response);
			break;
		}
	}

}
