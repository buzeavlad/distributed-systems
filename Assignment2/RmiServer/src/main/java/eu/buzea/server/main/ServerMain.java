package eu.buzea.server.main;

import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import eu.buzea.constants.Constants;
import eu.buzea.remotes.SellingServiceRemote;
import eu.buzea.remotes.TaxServiceRemote;
import eu.buzea.server.services.SellingServiceImpl;
import eu.buzea.server.services.TaxServiceImpl;

public class ServerMain {

	public static void main(String[] args) throws RemoteException, AlreadyBoundException {
		TaxServiceRemote taxService = new TaxServiceImpl();
		SellingServiceRemote sellingService = new SellingServiceImpl();
		Registry registry = LocateRegistry.createRegistry(Constants.PORT_NUMBER);
		registry.bind(Constants.TAX_SERVICE, taxService);
		registry.bind(Constants.SELLING_SERVICE, sellingService);
		System.out.println("Server started");
	}

}
