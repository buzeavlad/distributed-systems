/**
 * 
 */

function validateForm() {

	var departTime = document.forms["FlightForm"]["departTime"].value;
	var arrivalTime = document.forms["FlightForm"]["arrivalTime"].value;

	var e = document.forms["FlightForm"]["arivalCitySelectList"];
	var arrivalCity = e.options[e.selectedIndex].value;

	var i = document.forms["FlightForm"]["departCitySelectList"];
	var departCity = i.options[i.selectedIndex].value;

	
	if (departTime > arrivalTime) {
		alert("Invalid departure or arrival time");
		return false;
	}

	if (arrivalCity == departCity) {
		alert("A flight cannot depart and arrive at the same city");
		return false;
	}

}