<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<link rel="stylesheet" href="css/bootstrap.css" />
<link rel="stylesheet" href="css/bootstrap-theme.css" />
<title>Tracking System</title>
</head>
<body>
	<div id="myHeader" align="right">
		<form action="logIn">
			<input name="logOut" value="Log Out" type="submit" />
		</form>
		<form action="AdminActions">
			<input name="home" value="Home" type="submit">
		</form>
	</div>
	<div align="center">
		<h1>Add new Package</h1>
		<form action="AdminActions">
			<table>
				<tr>
					<td>Sender</td>
					<td><input name="sender"></td>
				</tr>
				<tr>
					<td>Receiver</td>
					<td><input name="receiver"></td>
				</tr>
				<tr>
					<td>From</td>
					<td><select name="from">
							<c:forEach var="c" items="${cities}">
								<option value="${c.name}">${c.name}</option>
							</c:forEach>
					</select></td>
				</tr>
				<tr>
					<td>To</td>
					<td><select name="to">
							<c:forEach var="c" items="${cities}">
								<option value="${c.name}">${c.name}</option>
							</c:forEach>
					</select></td>
				</tr>
				<tr>
					<td>Name</td>
					<td><input name="pkgName" type="text" /></td>
				</tr>
				<tr>
					<td>Description</td>
					<td><input name="description" type="text" /></td>
				</tr>
				<tr>
					<td><input name="addPackage" type="submit" /></td>
				</tr>
			</table>
		</form>
	</div>

</body>
</html>