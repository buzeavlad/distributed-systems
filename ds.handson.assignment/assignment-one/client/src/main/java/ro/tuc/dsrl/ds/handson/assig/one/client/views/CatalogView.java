package ro.tuc.dsrl.ds.handson.assig.one.client.views;

import java.awt.Color;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import ro.tuc.dsrl.ds.handson.assig.one.client.entities.Student;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description:
 *	CatalogView is a JFrame which contains the UI elements of the Client application.
 */
public class CatalogView extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textFirstname;
	private JTextField textLastname;
	private JTextField textMail;
	private JTextField textId;
	private JButton btnGet;
	private JButton btnPost;
	private JTextArea textArea;
	private JPanel deletePanel;
	private JButton btnDelete;
	private JSpinner spinnerId;

	public CatalogView() {
		setTitle("HTTP Protocol simulator");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(300, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblInsertNewStudent = new JLabel("Insert new student");
		lblInsertNewStudent.setBounds(10, 11, 120, 14);
		contentPane.add(lblInsertNewStudent);

		JLabel lblFirstname = new JLabel("First name");
		lblFirstname.setBounds(10, 36, 60, 14);
		contentPane.add(lblFirstname);

		JLabel lblLastname = new JLabel("Last name");
		lblLastname.setBounds(10, 61, 60, 14);
		contentPane.add(lblLastname);

		JLabel lblMail = new JLabel("Mail");
		lblMail.setBounds(10, 86, 46, 14);
		contentPane.add(lblMail);

		textFirstname = new JTextField();
		textFirstname.setBounds(80, 33, 86, 20);
		contentPane.add(textFirstname);
		textFirstname.setColumns(10);

		textLastname = new JTextField();
		textLastname.setBounds(80, 58, 86, 20);
		contentPane.add(textLastname);
		textLastname.setColumns(10);

		textMail = new JTextField();
		textMail.setBounds(80, 83, 86, 20);
		contentPane.add(textMail);
		textMail.setColumns(10);

		btnPost = new JButton("POST");
		btnPost.setBounds(10, 111, 89, 23);
		contentPane.add(btnPost);

		JLabel lblFindStudentBy = new JLabel("Find student by id ");
		lblFindStudentBy.setBounds(235, 11, 145, 14);
		contentPane.add(lblFindStudentBy);

		JLabel lblId = new JLabel("Id");
		lblId.setBounds(235, 36, 46, 14);
		contentPane.add(lblId);

		textId = new JTextField();
		textId.setBounds(320, 33, 86, 20);
		contentPane.add(textId);
		textId.setColumns(10);

		btnGet = new JButton("GET");
		btnGet.setBounds(235, 77, 89, 23);
		contentPane.add(btnGet);

		textArea = new JTextArea();
		textArea.setBounds(235, 131, 171, 120);
		contentPane.add(textArea);
		
		deletePanel = new JPanel();
		deletePanel.setBorder(new LineBorder(new Color(0, 0, 0)));
		deletePanel.setBounds(10, 145, 171, 106);
		contentPane.add(deletePanel);
		deletePanel.setLayout(null);
		
		JLabel lblDeleteStudentBy = new JLabel("Delete Student by id");
		lblDeleteStudentBy.setBounds(10, 11, 151, 14);
		deletePanel.add(lblDeleteStudentBy);
		
		JLabel lblId_1 = new JLabel("Id");
		lblId_1.setBounds(10, 36, 46, 14);
		deletePanel.add(lblId_1);
		
		spinnerId = new JSpinner();
		spinnerId.setModel(new SpinnerNumberModel(new Integer(1), new Integer(0), null, new Integer(1)));
		spinnerId.setBounds(115, 33, 46, 20);
		deletePanel.add(spinnerId);
		
		btnDelete = new JButton("Delete");
		btnDelete.setBounds(10, 72, 89, 23);
		deletePanel.add(btnDelete);
	}

	public void addBtnGetActionListener(ActionListener e) {
		btnGet.addActionListener(e);
	}

	public void addBtnPostActionListener(ActionListener e) {
		btnPost.addActionListener(e);
	}
	
	public void addBtnDeleteActionListener(ActionListener e){
		btnDelete.addActionListener(e);
	}
	
	public int getSpinnerValue(){
		int value =(int)spinnerId.getValue();
		return value;
	}

	public String getStudentId() {
		return textId.getText();
	}

	public String getFirstname() {
		return textFirstname.getText();
	}

	public String getLastname() {
		return textLastname.getText();
	}

	public String getMail() {
		return textMail.getText();
	}

	public void printStudent(Student student) {
		textArea.setText(student.toString());
	}

	public void clear() {
		textId.setText("");
		textFirstname.setText("");
		textLastname.setText("");
		textMail.setText("");
		spinnerId.setValue(1);
	}
}
