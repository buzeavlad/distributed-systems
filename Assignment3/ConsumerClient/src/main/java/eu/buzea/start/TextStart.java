package eu.buzea.start;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import javax.jms.JMSException;

import eu.buzea.consumers.TextConsumer;

public class TextStart {

	
	public static void main(String[] args) throws IOException, TimeoutException, JMSException {
		TextConsumer consumer = new TextConsumer("jms/textQueue");
		while(true){
			consumer.receiveDvd();
		}
	}
}
