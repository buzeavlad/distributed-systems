package eu.buzea.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

public class GenericDao<T> {

	protected EntityManager entityManager;
	protected Class<T> typeClass;

	public GenericDao(Class<T> typeParameterClass) {
		EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("DataLayerModule");
		entityManager = entityManagerFactory.createEntityManager();
		typeClass = typeParameterClass;
	}

	public void create(T t) {
		entityManager.getTransaction().begin();
		entityManager.persist(t);
		entityManager.getTransaction().commit();
	}

	public T find(int id) {
		entityManager.getTransaction().begin();
		T found = entityManager.find(typeClass, id);
		entityManager.getTransaction().commit();
		return found;
	}

	public void delete(int id) {
		entityManager.getTransaction().begin();
		T t = entityManager.find(typeClass, id);
		entityManager.remove(t);
		entityManager.getTransaction().commit();
	}

	public List<T> findAll() {
		entityManager.getTransaction().begin();
		String query = "Select e from " + typeClass.getName() + " e";
		TypedQuery<T> q = entityManager.createQuery(query, typeClass);
		List<T> result = q.getResultList();
		entityManager.getTransaction().commit();
		return result;
	}

}
