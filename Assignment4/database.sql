CREATE DATABASE  IF NOT EXISTS `tracking-system` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `tracking-system`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: localhost    Database: tracking-system
-- ------------------------------------------------------
-- Server version	5.6.27-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city` (
  `idCity` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(45) NOT NULL,
  `Country` varchar(45) NOT NULL,
  `latitute` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  PRIMARY KEY (`idCity`),
  UNIQUE KEY `Name_UNIQUE` (`Name`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city`
--

LOCK TABLES `city` WRITE;
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
INSERT INTO `city` VALUES (1,'Cluj-Napoca','Romania',NULL,NULL),(2,'Iasi','Romania',NULL,NULL),(3,'Bucuresti','Romania',NULL,NULL),(4,'Timisoara','Romania',NULL,NULL),(5,'Brasov','Romania',NULL,NULL),(6,'Constanta','Romania',NULL,NULL),(7,'Targu Jiu','Romania',NULL,NULL),(8,'Petrosani','Romania',NULL,NULL);
/*!40000 ALTER TABLE `city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `package`
--

DROP TABLE IF EXISTS `package`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `package` (
  `idpackage` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(45) DEFAULT NULL,
  `status` enum('SENT','DELIVERED','PENDING') NOT NULL DEFAULT 'PENDING',
  `tracking` bit(1) NOT NULL DEFAULT b'0',
  `sender` int(11) NOT NULL,
  `receiver` int(11) NOT NULL,
  `senderCity` int(11) NOT NULL,
  `destinationCity` int(11) NOT NULL,
  PRIMARY KEY (`idpackage`),
  UNIQUE KEY `name_UNIQUE` (`name`),
  KEY `source_idx` (`senderCity`),
  KEY `destination_idx` (`destinationCity`),
  KEY `sender_idx` (`sender`),
  KEY `receiver_idx` (`receiver`),
  CONSTRAINT `destination` FOREIGN KEY (`destinationCity`) REFERENCES `city` (`idCity`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `receiver` FOREIGN KEY (`receiver`) REFERENCES `user` (`idUser`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `sender` FOREIGN KEY (`sender`) REFERENCES `user` (`idUser`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `source` FOREIGN KEY (`senderCity`) REFERENCES `city` (`idCity`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `package`
--

LOCK TABLES `package` WRITE;
/*!40000 ALTER TABLE `package` DISABLE KEYS */;
INSERT INTO `package` VALUES (14,'newPackage','','DELIVERED','',4,5,1,3),(23,'test package','description','PENDING','\0',4,5,1,3),(26,'Mir','Donatie','PENDING','\0',8,9,1,2),(33,'Aur','donatie','SENT','',8,9,1,3),(36,'Retur Aur','','DELIVERED','\0',9,8,3,1),(37,'$$$','Multi bani','PENDING','\0',11,9,3,2);
/*!40000 ALTER TABLE `package` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `routeentry`
--

DROP TABLE IF EXISTS `routeentry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `routeentry` (
  `idrouteEntry` int(11) NOT NULL AUTO_INCREMENT,
  `package` int(11) NOT NULL,
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `location` int(11) NOT NULL,
  PRIMARY KEY (`idrouteEntry`),
  KEY `place_idx` (`location`),
  KEY `package_idx` (`package`),
  CONSTRAINT `package` FOREIGN KEY (`package`) REFERENCES `package` (`idpackage`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `place` FOREIGN KEY (`location`) REFERENCES `city` (`idCity`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `routeentry`
--

LOCK TABLES `routeentry` WRITE;
/*!40000 ALTER TABLE `routeentry` DISABLE KEYS */;
INSERT INTO `routeentry` VALUES (11,14,'2015-12-22 14:30:43',2),(12,14,'2016-01-01 00:00:00',1),(13,14,'2016-01-01 00:00:00',7),(14,14,'2016-01-01 00:00:00',1),(15,14,'2016-01-01 03:04:00',6),(16,14,'2016-01-01 03:04:00',6),(17,14,'2016-01-01 03:04:00',6),(20,33,'2016-01-01 00:00:00',1),(21,33,'2016-01-01 17:00:00',3);
/*!40000 ALTER TABLE `routeentry` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `idUser` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `isAdmin` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`idUser`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (4,'dsTest','root','\0'),(5,'receiverTest','password','\0'),(6,'admin','admin',''),(7,'root','root',''),(8,'Gigel','Frone','\0'),(9,'Daniel','PFA','\0'),(11,'Becali','George','\0');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-03 12:22:57
