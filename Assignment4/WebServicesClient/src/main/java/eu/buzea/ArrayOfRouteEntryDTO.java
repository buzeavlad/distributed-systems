
package eu.buzea;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOfRouteEntryDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfRouteEntryDTO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RouteEntryDTO" type="{http://buzea.eu/}RouteEntryDTO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfRouteEntryDTO", propOrder = {
    "routeEntryDTO"
})
public class ArrayOfRouteEntryDTO {

    @XmlElement(name = "RouteEntryDTO", nillable = true)
    protected List<RouteEntryDTO> routeEntryDTO;

    /**
     * Gets the value of the routeEntryDTO property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the routeEntryDTO property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRouteEntryDTO().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RouteEntryDTO }
     * 
     * 
     */
    public List<RouteEntryDTO> getRouteEntryDTO() {
        if (routeEntryDTO == null) {
            routeEntryDTO = new ArrayList<RouteEntryDTO>();
        }
        return this.routeEntryDTO;
    }

}
