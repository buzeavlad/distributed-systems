package ro.tuc.dsrl.ds.handson.assig.three.consumer.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class TextService {

	public static final String BASE_FOLDER = "./TextOutput";

	private int msgNumber;

	public TextService() {
		msgNumber = 0;
	}

	public boolean writeTextToFile(String msg) {
		File msgFile = new File(BASE_FOLDER, "Message_" + msgNumber + ".txt");
		msgFile.getParentFile().mkdirs();
		PrintWriter printWriter = null;
		try {
			printWriter = new PrintWriter(msgFile);
			printWriter.write(msg);
			msgNumber++;
			return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (printWriter != null)
				printWriter.close();
		}
		return false;
	}

}
