package ro.tuc.dsrl.ds.handson.assig.three.consumer.start;

import java.io.IOException;

import ro.tuc.dsrl.ds.handson.assig.three.consumer.connection.QueueServerConnection;
import ro.tuc.dsrl.ds.handson.assig.three.consumer.service.TextService;
import ro.tuc.dsrl.ds.handson.assig.three.entities.Dvd;

public class TextClientStart {

	private TextClientStart() {

	}

	public static void main(String[] args) {
		QueueServerConnection queue = new QueueServerConnection("localhost", 8888);

		TextService textService = new TextService();
		String message;

		while (true) {
			try {
				message = queue.readMessage();
				Dvd received = Dvd.deserialize(message);
				if (received != null) {
					System.out.println("Writing DVD to file");
					textService.writeTextToFile(received.toString());
				} else {
					System.out.println("Writing text message to file");
					textService.writeTextToFile(message);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
