package eu.buzea.consumers;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.ObjectMessage;

import eu.buzea.dvd.Dvd;
import eu.buzea.messaging.JmsEndPoint;
import eu.buzea.services.TextService;

public class TextConsumer extends JmsEndPoint {

	private TextService textService;
	private MessageConsumer myMsgConsumer;

	public TextConsumer(String queueName) {
		super(queueName);

		textService = new TextService();
		try {
			myMsgConsumer = mySess.createConsumer(myQueue);
		} catch (JMSException e) {

			e.printStackTrace();
		}
	}

	public void receiveDvd() throws JMSException {
		Message msg = myMsgConsumer.receive();
		if (msg instanceof ObjectMessage) {
			ObjectMessage txtMsg = (ObjectMessage) msg;
			Dvd dvd = (Dvd) txtMsg.getObject();
			textService.writeDvdToFile(dvd);
		}
	}

}
