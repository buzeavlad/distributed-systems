package eu.buzea;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ClientActions
 */
@WebServlet("/ClientActions")
public class ClientActions extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ClientActions() {
		super();

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		throw new IllegalAccessError();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String confirm = request.getParameter("confirm");
		String pkgName = request.getParameter("pkgName");
		String username = (String) request.getSession().getAttribute("user");
		String viewRoute = request.getParameter("viewRoute");
		String search = request.getParameter("search");
		// String home = request.getParameter("home");
		ClientWsSoap clientWs = new ClientWs().getClientWsSoap();
		if (confirm != null) {
			clientWs.confirmDelivery(pkgName);
			List<PackageDTO> packages = clientWs.listAllPackages(username).getPackageDTO();
			request.setAttribute("packageList", packages);
			request.getRequestDispatcher("./client.jsp").forward(request, response);
		} else if (viewRoute != null) {
			List<RouteEntryDTO> route = clientWs.getRoute(pkgName).getRouteEntryDTO();
			request.setAttribute("route", route);
			request.getRequestDispatcher("./viewRoute.jsp").forward(request, response);

		} else if (search != null) {
			String searchBox = request.getParameter("searchBox");
			List<PackageDTO> packages = clientWs.searchPackage(searchBox, username).getPackageDTO();
			request.setAttribute("packageList", packages);
			request.getRequestDispatcher("./client.jsp").forward(request, response);
		} else {
			List<PackageDTO> packages = clientWs.listAllPackages(username).getPackageDTO();
			request.setAttribute("packageList", packages);
			request.getRequestDispatcher("./client.jsp").forward(request, response);
		}
	}

}
