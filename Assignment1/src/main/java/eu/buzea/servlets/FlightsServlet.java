package eu.buzea.servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import eu.buzea.dao.*;
import eu.buzea.model.*;

/**
 * Servlet implementation class FlightsServlet
 */
@WebServlet("/FlightsServlet")
public class FlightsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm");

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public FlightsServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.sendRedirect("index.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		FlightDao flightDao = new FlightDao();
		CityDao cityDao = new CityDao();
		AirplaneDao airplaneDao = new AirplaneDao();
		
		String buttonPressed = request.getParameter("submit");
		Flight flight = new Flight();
		
		
		String airplaneId = request.getParameter("airplaneTypeSelectList");
		int airplaneIdInt = Integer.parseInt(airplaneId);
		Airplane airplane = airplaneDao.find(airplaneIdInt);
		flight.setAirplane(airplane);
		
		String departureCityId = request.getParameter("departCitySelectList");
		int departureCityIdInt = Integer.parseInt(departureCityId);
		City departureCity = cityDao.find(departureCityIdInt);
		flight.setCityByDepartureCity(departureCity);
		
		String departTime = request.getParameter("departTime");
		departTime=departTime.replace("T", " ");
		try {
			Date departDate = DATE_FORMAT.parse(departTime);
			flight.setDepartureTime(departDate);
		} catch (ParseException e) {
			// should not happen
			e.printStackTrace();
		}
		
		
		String arrivalCityId = request.getParameter("arivalCitySelectList");
		int arrivalCityIdInt = Integer.parseInt(arrivalCityId);
		City arrivalCity = cityDao.find(arrivalCityIdInt);
		flight.setCityByArrivalCity(arrivalCity);
		
		String arrivalTime = request.getParameter("arrivalTime");
		arrivalTime=arrivalTime.replace("T", " ");
		System.out.println(arrivalTime);
		try {
			Date arrivalDate = DATE_FORMAT.parse(arrivalTime);
			flight.setArrivalTime(arrivalDate);
		} catch (ParseException e) {
			// should never happen
			e.printStackTrace();
		}
		
		if (buttonPressed.equals("Update flight")) {
			String flightNumber = request.getParameter("flightNumber");
			int flightNumberInt = Integer.parseInt(flightNumber);
			flightDao.update(flightNumberInt, flight);
			
		} else {
			flightDao.create(flight);
		}
		
		List<Flight> flights=flightDao.findAll();
		request.setAttribute("flights",flights );
		request.getRequestDispatcher("admin.jsp").forward(request, response);

	}

}
