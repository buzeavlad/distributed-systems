package eu.buzea.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import eu.buzea.dao.AdminDao;
import eu.buzea.dao.FlightDao;
import eu.buzea.dao.UserDao;
import eu.buzea.model.Admin;
import eu.buzea.model.Flight;
import eu.buzea.model.User;

/**
 * Servlet implementation class LoginServlet
 */
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private AdminDao adminDao;
	private UserDao dao;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginServlet() {
		super();
	}

	@Override
	public void init() throws ServletException {
		super.init();
		dao = new UserDao();
		adminDao = new AdminDao();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String logOut =request.getParameter("logOut");
		if(logOut!=null){
			request.getSession().invalidate();
			response.sendRedirect("index.jsp");
		}else{
		String username = request.getParameter("username");
		String password = request.getParameter("password");

		User user = dao.findUser(username);
		Admin admin = adminDao.find(username);
		if (user != null && user.getPassword().equals(password)) {
			Cookie userCookie = new Cookie("user", username);
			userCookie.setMaxAge(30 * 60);// 30 mins
			response.addCookie(userCookie);
			request.getSession().setAttribute("user", user);
			request.setAttribute("bookings", user.getBookings());
			request.getRequestDispatcher("user.jsp").forward(request, response);
		} else {

			if (admin != null && admin.getPassword().equals(password)) {
				Cookie userCookie = new Cookie("user", "admin");
				userCookie.setMaxAge(30 * 60);// 30 mins
				response.addCookie(userCookie);
				request.getSession().setAttribute("admin", admin);
				FlightDao flightDao = new FlightDao();
				List<Flight> flights = flightDao.findAll();
				request.setAttribute("flights", flights);
				request.getRequestDispatcher("admin.jsp").forward(request, response);
			} else {
				request.setAttribute("msg", "Invalid login credentials");
				request.getRequestDispatcher("index.jsp").forward(request, response);
			}
		}
		}
	}

}
