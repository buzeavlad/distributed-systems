package eu.buzea.constants;

public class Constants {
	public static final int PORT_NUMBER = 8889;

	public static final String TAX_SERVICE = "TAX_SERVICE";
	
	public static final String SELLING_SERVICE = "SELLING_SERVICE";

}
