package eu.buzea.messaging;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public abstract class RabbitMQEndPoint {

	protected String queueName;
	protected Channel channel;
	protected Connection connection;

	protected RabbitMQEndPoint(String queue_name) throws IOException, TimeoutException {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("localhost");
		connection = null;
		channel = null;
		connection = factory.newConnection();
		channel = connection.createChannel();
		channel.queueDeclare(queue_name, false, false, false, null);
		this.queueName=queue_name;

	}

	public void closeAll() {
		if (channel != null) {
			try {
				channel.close();
			} catch (IOException | TimeoutException e) {
				e.printStackTrace();
			}
		}
		if (connection != null) {
			try {
				connection.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
