
package eu.buzea.ws.admin;

import java.util.List;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebService(name = "AdminWebService", targetNamespace = "http://admin.ws.buzea.eu/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface AdminWebService {


    /**
     * 
     * @param arg0
     * @return
     *     returns eu.buzea.ws.admin.PackageItem
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "findByName", targetNamespace = "http://admin.ws.buzea.eu/", className = "eu.buzea.ws.admin.FindByName")
    @ResponseWrapper(localName = "findByNameResponse", targetNamespace = "http://admin.ws.buzea.eu/", className = "eu.buzea.ws.admin.FindByNameResponse")
    @Action(input = "http://admin.ws.buzea.eu/AdminWebService/findByNameRequest", output = "http://admin.ws.buzea.eu/AdminWebService/findByNameResponse")
    public PackageItem findByName(
        @WebParam(name = "arg0", targetNamespace = "")
        String arg0);

    /**
     * 
     * @param arg0
     */
    @WebMethod
    @RequestWrapper(localName = "setDelivered", targetNamespace = "http://admin.ws.buzea.eu/", className = "eu.buzea.ws.admin.SetDelivered")
    @ResponseWrapper(localName = "setDeliveredResponse", targetNamespace = "http://admin.ws.buzea.eu/", className = "eu.buzea.ws.admin.SetDeliveredResponse")
    @Action(input = "http://admin.ws.buzea.eu/AdminWebService/setDeliveredRequest", output = "http://admin.ws.buzea.eu/AdminWebService/setDeliveredResponse")
    public void setDelivered(
        @WebParam(name = "arg0", targetNamespace = "")
        String arg0);

    /**
     * 
     * @return
     *     returns java.util.List<eu.buzea.ws.admin.PackageItem>
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "findAllPackages", targetNamespace = "http://admin.ws.buzea.eu/", className = "eu.buzea.ws.admin.FindAllPackages")
    @ResponseWrapper(localName = "findAllPackagesResponse", targetNamespace = "http://admin.ws.buzea.eu/", className = "eu.buzea.ws.admin.FindAllPackagesResponse")
    @Action(input = "http://admin.ws.buzea.eu/AdminWebService/findAllPackagesRequest", output = "http://admin.ws.buzea.eu/AdminWebService/findAllPackagesResponse")
    public List<PackageItem> findAllPackages();

    /**
     * 
     * @param arg0
     */
    @WebMethod
    @RequestWrapper(localName = "enableTracking", targetNamespace = "http://admin.ws.buzea.eu/", className = "eu.buzea.ws.admin.EnableTracking")
    @ResponseWrapper(localName = "enableTrackingResponse", targetNamespace = "http://admin.ws.buzea.eu/", className = "eu.buzea.ws.admin.EnableTrackingResponse")
    @Action(input = "http://admin.ws.buzea.eu/AdminWebService/enableTrackingRequest", output = "http://admin.ws.buzea.eu/AdminWebService/enableTrackingResponse")
    public void enableTracking(
        @WebParam(name = "arg0", targetNamespace = "")
        String arg0);

    /**
     * 
     * @param arg2
     * @param arg1
     * @param arg0
     * @return
     *     returns boolean
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "addRouteEntry", targetNamespace = "http://admin.ws.buzea.eu/", className = "eu.buzea.ws.admin.AddRouteEntry")
    @ResponseWrapper(localName = "addRouteEntryResponse", targetNamespace = "http://admin.ws.buzea.eu/", className = "eu.buzea.ws.admin.AddRouteEntryResponse")
    @Action(input = "http://admin.ws.buzea.eu/AdminWebService/addRouteEntryRequest", output = "http://admin.ws.buzea.eu/AdminWebService/addRouteEntryResponse")
    public boolean addRouteEntry(
        @WebParam(name = "arg0", targetNamespace = "")
        String arg0,
        @WebParam(name = "arg1", targetNamespace = "")
        String arg1,
        @WebParam(name = "arg2", targetNamespace = "")
        XMLGregorianCalendar arg2);

    /**
     * 
     * @return
     *     returns java.util.List<eu.buzea.ws.admin.City>
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "findAllCities", targetNamespace = "http://admin.ws.buzea.eu/", className = "eu.buzea.ws.admin.FindAllCities")
    @ResponseWrapper(localName = "findAllCitiesResponse", targetNamespace = "http://admin.ws.buzea.eu/", className = "eu.buzea.ws.admin.FindAllCitiesResponse")
    @Action(input = "http://admin.ws.buzea.eu/AdminWebService/findAllCitiesRequest", output = "http://admin.ws.buzea.eu/AdminWebService/findAllCitiesResponse")
    public List<City> findAllCities();

    /**
     * 
     * @param arg0
     */
    @WebMethod
    @RequestWrapper(localName = "deletePackage", targetNamespace = "http://admin.ws.buzea.eu/", className = "eu.buzea.ws.admin.DeletePackage")
    @ResponseWrapper(localName = "deletePackageResponse", targetNamespace = "http://admin.ws.buzea.eu/", className = "eu.buzea.ws.admin.DeletePackageResponse")
    @Action(input = "http://admin.ws.buzea.eu/AdminWebService/deletePackageRequest", output = "http://admin.ws.buzea.eu/AdminWebService/deletePackageResponse")
    public void deletePackage(
        @WebParam(name = "arg0", targetNamespace = "")
        String arg0);

    /**
     * 
     * @param arg0
     */
    @WebMethod
    @RequestWrapper(localName = "createPackage", targetNamespace = "http://admin.ws.buzea.eu/", className = "eu.buzea.ws.admin.CreatePackage")
    @ResponseWrapper(localName = "createPackageResponse", targetNamespace = "http://admin.ws.buzea.eu/", className = "eu.buzea.ws.admin.CreatePackageResponse")
    @Action(input = "http://admin.ws.buzea.eu/AdminWebService/createPackageRequest", output = "http://admin.ws.buzea.eu/AdminWebService/createPackageResponse")
    public void createPackage(
        @WebParam(name = "arg0", targetNamespace = "")
        PackageDto arg0);

    /**
     * 
     * @param arg0
     * @return
     *     returns java.util.List<eu.buzea.ws.admin.RouteEntry>
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getRoute", targetNamespace = "http://admin.ws.buzea.eu/", className = "eu.buzea.ws.admin.GetRoute")
    @ResponseWrapper(localName = "getRouteResponse", targetNamespace = "http://admin.ws.buzea.eu/", className = "eu.buzea.ws.admin.GetRouteResponse")
    @Action(input = "http://admin.ws.buzea.eu/AdminWebService/getRouteRequest", output = "http://admin.ws.buzea.eu/AdminWebService/getRouteResponse")
    public List<RouteEntry> getRoute(
        @WebParam(name = "arg0", targetNamespace = "")
        String arg0);

}
