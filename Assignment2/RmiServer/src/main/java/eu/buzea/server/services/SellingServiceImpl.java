package eu.buzea.server.services;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

import eu.buzea.entities.Car;
import eu.buzea.remotes.SellingServiceRemote;

public class SellingServiceImpl extends UnicastRemoteObject implements SellingServiceRemote {

	public SellingServiceImpl() throws RemoteException {
		super();
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 7861185821178614136L;

	@Override
	public double calculateSellingPrice(Car car) throws RemoteException {
		double purchasingPrice = car.getPrice();
		int carYear = car.getYear();
		double sellingPrice = purchasingPrice - (purchasingPrice / 7) * (2015 - carYear);
		if (sellingPrice < 0) {
			sellingPrice = 0.0;
		}
		return sellingPrice;
	}

}
