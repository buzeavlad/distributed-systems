package eu.buzea.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import eu.buzea.model.Booking;
import eu.buzea.util.HibernateUtil;

public class BookingDao {

	public BookingDao() {
		
	}
	
	public void create(Booking booking) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			int id = (Integer) session.save(booking);
			booking.setIdbooking(id);
			tx.commit();

		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
	}

}
