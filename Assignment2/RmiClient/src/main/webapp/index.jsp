<?xml version="1.0" encoding="ISO-8859-1" ?>
<jsp:root xmlns:jsp="http://java.sun.com/JSP/Page" version="2.0">
	<jsp:directive.page contentType="text/html; charset=ISO-8859-1"
		pageEncoding="ISO-8859-1" session="false" />
	<jsp:output doctype-root-element="html"
		doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"
		doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
		omit-xml-declaration="true" />
	<html>
<head>
<LINK rel="stylesheet" href="css/bootstrap.css" />
<LINK rel="stylesheet" href="css/bootstrap-theme.css" />
<title>Car Calculations Service</title>
<SCRIPT type="text/javascript">
	window.onload = function() {
		var sellingPrice = '${sellingPrice}';
		var tax = '${tax}';
		if (sellingPrice != '') {
			var message = "You can sell your car for $".concat(sellingPrice);
			alert(message);
		}

		if (tax != '') {
			var message = "The tax for your car is ".concat(tax);
			alert(message);
		}
	}
</SCRIPT>
</head>
<body>
	<DIV align="center">
		<H1>Car Calculations Service</H1>
		<FORM action="CarCalculationServlet">
			<table border="1">
				<tr>
					<TH>Property</TH>
					<TH>Value</TH>
				</tr>
				<tr>
					<td /> Engine Capacity
					<td />
					<input type="number" name="engineCapacity" min="100" max="10000"
						step="100" value="2000"/>
				</tr>
				<TR>
					<TD />Fabrication year
					<TD />
					<input type="number" name="year" min="1900" max="2015" step="1"
						value="2000" />
				</TR>
				<TR>
					<TD>The price for <BR />which you bought the car
					</TD>
					<TD />
					<input type="number" name="price" min="0" max="10000000" step="500"
						value="1000"/>$
				</TR>
				<tr>
					<TD />
					<button type="submit" name="buttonPressed" width="100%" value="Calculate Tax">Calculate
						Tax</button>
					<TD />
					<button type="submit" name="buttonPressed" width="100%" value="Calculate Selling Price">Calculate
						Selling Price</button>
				</tr>
			</table>
		</FORM>
	</DIV>
</body>
	</html>
</jsp:root>