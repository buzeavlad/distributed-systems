package eu.buzea.dvd;

import java.io.Serializable;

public class Dvd implements Serializable {
	private static final long serialVersionUID = 1L;
	private String artist, albumName;
	private double price;
	private int year;
	public Dvd(String artist, String albumName, double price, int year) {
		super();
		this.artist = artist;
		this.albumName = albumName;
		this.price = price;
		this.year = year;
	}
	public String getArtist() {
		return artist;
	}
	public void setArtist(String artist) {
		this.artist = artist;
	}
	public String getAlbumName() {
		return albumName;
	}
	public void setAlbumName(String albumName) {
		this.albumName = albumName;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	@Override
	public String toString() {
		return "Dvd [artist=" + artist + ", albumName=" + albumName + ", price=" + price + ", year=" + year + "]";
	}
	
	

}
