package ro.tuc.dsrl.ds.handson.assig.three.entities;

public class Dvd {

	private String title;
	private int year;
	private double price;

	public Dvd(String title, int year, double price) {
		super();
		this.title = title;
		this.year = year;
		this.price = price;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public static String serialize(Dvd dvd) {
		return dvd.getTitle() + "&" + dvd.getPrice() + "&" + dvd.getYear();
	}

	public static Dvd deserialize(String dvd) {
		String[] sections = dvd.split("&");
		Dvd value = null;
		if (sections.length == 3) {
			String title = sections[0];
			double price = Double.parseDouble(sections[1]);
			int year = Integer.parseInt(sections[2]);
			value = new Dvd(title, year, price);
		}
		return value;
	}

	@Override
	public String toString() {
		return "Dvd \ntitle=" + title + "\nyear=" + year + "\nprice=" + price + "\n";
	}
	
	

}
