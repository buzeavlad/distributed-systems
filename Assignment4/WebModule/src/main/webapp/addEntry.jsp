<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="css/bootstrap.css" />
<link rel="stylesheet" href="css/bootstrap-theme.css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Tracking System</title>
</head>
<body>
	<div id="myHeader" align="right">
		<form action="logIn">
			<input name="logOut" value="Log Out" type="submit" />
		</form>
		<form action="AdminActions">
			<input name="home" value="Home" type="submit">
		</form>
	</div>
	
	<div align="center">
		<H1>Entry List</H1>
		<table border="1">
			<tr>
				<TH>City</TH>
				<TH>Time</TH>
			</tr>
			<c:forEach var="r" items="${route}">
				<tr>
					<td>${r.city.name}</td>
					<td>${r.time}</td>
				</tr>
			</c:forEach>
		</table>
		<c:choose>
			<c:when test="${item.status!='DELIVERED' }">
				<h2>Add new entry</h2>
				<form action="AdminActions">
					<input type="hidden" name="pkgName" value="${item.name}" />
					<table border="1">
						<tr>
							<td><select name="city">
									<c:forEach var="c" items="${cities}">
										<option value="${c.name}">${c.name}</option>
									</c:forEach>
							</select></td>
							<td><input type="datetime-local" name="time"
								value="2016-01-01T00:00:00.00" /></td>
							<td><input type="submit" name="submitEntry" value="Submit" /></td>
						</tr>
					</table>
				</form>
			</c:when>
			<c:otherwise>
				<h2>This package has already been delivered!</h2>
			</c:otherwise>
		</c:choose>
	</div>
</body>
</html>