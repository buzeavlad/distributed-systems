<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>DS Airport</title>
<link rel="stylesheet" href="css/bootstrap.css" />
<link rel="stylesheet" href="css/bootstrap-theme.css" />
</head>
<body>
	<div class="panel">
		<h1>Welcome to DS International Aiport!</h1>
		<p>
			Please log in with your user account or <a href="signup.jsp">sign
				up</a> for free
		</p>
		<form action="LoginServlet" method="post">
			<input type="text" name="username"></input><br /> <input
				type="password" name="password"></input><br /> <input type="submit"
				value="Log In" /><BR />
			<label>${msg}</label>
		</form>
	</div>
</body>
</html>