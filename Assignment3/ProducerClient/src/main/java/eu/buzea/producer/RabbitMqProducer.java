package eu.buzea.producer;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import org.apache.commons.lang.SerializationUtils;

import eu.buzea.dvd.Dvd;
import eu.buzea.messaging.RabbitMQEndPoint;

public class RabbitMqProducer extends RabbitMQEndPoint {


	public RabbitMqProducer(String queueName) throws IOException, TimeoutException {
		super(queueName);
	}

	
	public boolean sendDvd(Dvd dvd) {
		try {
			channel.basicPublish("", queueName, null, SerializationUtils.serialize(dvd));
			
			return true;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return false;
	}

}
