
package eu.buzea.ws.admin;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for routeEntry complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="routeEntry">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="city" type="{http://admin.ws.buzea.eu/}city" minOccurs="0"/>
 *         &lt;element name="idrouteEntry" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="packageTracked" type="{http://admin.ws.buzea.eu/}packageItem" minOccurs="0"/>
 *         &lt;element name="time" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "routeEntry", propOrder = {
    "city",
    "idrouteEntry",
    "packageTracked",
    "time"
})
public class RouteEntry {

    protected City city;
    protected int idrouteEntry;
    protected PackageItem packageTracked;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar time;

    /**
     * Gets the value of the city property.
     * 
     * @return
     *     possible object is
     *     {@link City }
     *     
     */
    public City getCity() {
        return city;
    }

    /**
     * Sets the value of the city property.
     * 
     * @param value
     *     allowed object is
     *     {@link City }
     *     
     */
    public void setCity(City value) {
        this.city = value;
    }

    /**
     * Gets the value of the idrouteEntry property.
     * 
     */
    public int getIdrouteEntry() {
        return idrouteEntry;
    }

    /**
     * Sets the value of the idrouteEntry property.
     * 
     */
    public void setIdrouteEntry(int value) {
        this.idrouteEntry = value;
    }

    /**
     * Gets the value of the packageTracked property.
     * 
     * @return
     *     possible object is
     *     {@link PackageItem }
     *     
     */
    public PackageItem getPackageTracked() {
        return packageTracked;
    }

    /**
     * Sets the value of the packageTracked property.
     * 
     * @param value
     *     allowed object is
     *     {@link PackageItem }
     *     
     */
    public void setPackageTracked(PackageItem value) {
        this.packageTracked = value;
    }

    /**
     * Gets the value of the time property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTime() {
        return time;
    }

    /**
     * Sets the value of the time property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTime(XMLGregorianCalendar value) {
        this.time = value;
    }

}
