﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClientWebService
{
    public class RouteEntryDTO
    {
        private String packageName, time, location;

        public string Location
        {
            get
            {
                return location;
            }

            set
            {
                location = value;
            }
        }

        public string PackageName
        {
            get
            {
                return packageName;
            }

            set
            {
                packageName = value;
            }
        }

        public string Time
        {
            get
            {
                return time;
            }

            set
            {
                time = value;
            }
        }
    }
}