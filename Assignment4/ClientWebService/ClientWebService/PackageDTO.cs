﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ClientWebService
{
    public class PackageDTO
    {
        int id;
        String description, name, status, senderUsername, receiverUsername, senderCity, destinationCity;
        bool tracking;


        public string Description
        {
            get
            {
                return description;
            }

            set
            {
                description = value;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }

            set
            {
                name = value;
            }
        }

        public string Status
        {
            get
            {
                return status;
            }

            set
            {
                status = value;
            }
        }

        public string SenderUsername
        {
            get
            {
                return senderUsername;
            }

            set
            {
                senderUsername = value;
            }
        }

        public string ReceiverUsername
        {
            get
            {
                return receiverUsername;
            }

            set
            {
                receiverUsername = value;
            }
        }

        public string SenderCity
        {
            get
            {
                return senderCity;
            }

            set
            {
                senderCity = value;
            }
        }

        public string DestinationCity
        {
            get
            {
                return destinationCity;
            }

            set
            {
                destinationCity = value;
            }
        }

        public bool Tracking
        {
            get
            {
                return tracking;
            }

            set
            {
                tracking = value;
            }
        }

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }
    }
}