package eu.buzea.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * The persistent class for the routeentry database table.
 * 
 */
@Entity
@Table(name = "routeentry")
@NamedQuery(name = "RouteEntry.findAll", query = "SELECT r FROM RouteEntry r")
public class RouteEntry implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private int idrouteEntry;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	private Date time;

	// bi-directional many-to-one association to PackageItem
	@ManyToOne
	@JoinColumn(name = "package", nullable = false)
	private PackageItem packageTracked;

	// bi-directional many-to-one association to City
	@ManyToOne
	@JoinColumn(name = "location", nullable = false)
	private City city;

	public RouteEntry() {
	}

	public int getIdrouteEntry() {
		return this.idrouteEntry;
	}

	public void setIdrouteEntry(int idrouteEntry) {
		this.idrouteEntry = idrouteEntry;
	}

	public Date getTime() {
		return this.time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public PackageItem getPackageTracked() {
		return packageTracked;
	}

	public void setPackageTracked(PackageItem packageTracked) {
		this.packageTracked = packageTracked;
	}

	public City getCity() {
		return this.city;
	}

	public void setCity(City city) {
		this.city = city;
	}

}