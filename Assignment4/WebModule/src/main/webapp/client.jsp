<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>
<head>
<link rel="stylesheet" href="css/bootstrap.css" />
<link rel="stylesheet" href="css/bootstrap-theme.css" />
<title>Tracking System</title>
</head>
<body>
	<div id="myHeader" align="right">
		<div>
			<form action="logIn">
				<input name="logOut" value="Log Out" type="submit" />
			</form>
		</div>
		<div>
			<form action="ClientActions" method="post">
				<input type="text" name="searchBox" placeholder="Search" /> <input
					type="submit" value="Search" name="search">
			</form>
		</div>


	</div>
	<div align="center">
		<H1>Welcome ${sessionScope.user }</H1>
		<h2>List of packages</h2>
		<table border="1">
			<tr>
				<TH colspan="9">Package list</TH>
			</tr>
			<tr>
				<TH>Name</TH>
				<TH>Description</TH>
				<TH>Sender</TH>
				<TH>Receiver</TH>
				<TH>From</TH>
				<TH>To</TH>
				<TH>Status</TH>
				<TH>Tracking</TH>
				<TH>Actions</TH>
			</tr>
			<c:forEach var="pkg" items="${packageList}">
				<tr>
					<td>${pkg.name}</td>
					<td>${pkg.description}</td>
					<td>${pkg.senderUsername}</td>
					<td>${pkg.receiverUsername}</td>
					<td>${pkg.senderCity}</td>
					<td>${pkg.destinationCity}</td>
					<td>${pkg.status}</td>
					<td>${pkg.tracking}</td>
					<td><form action="ClientActions" method="post">
							<input type="hidden" name="pkgName" value="${pkg.name}" /> <input
								type="submit" name="viewRoute" value="View Route">
							<c:if test="${pkg.status != 'DELIVERED'}">
								<input type="submit" name="confirm" value="Confirm" />
							</c:if>

						</form></td>

				</tr>

			</c:forEach>
		</table>
	</div>

</body>
</html>