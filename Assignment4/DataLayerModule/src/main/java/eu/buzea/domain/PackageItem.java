package eu.buzea.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * The persistent class for the package database table.
 * 
 */
@Entity
@Table(name = "package")
@NamedQuery(name = "PackageItem.findAll", query = "SELECT p FROM PackageItem p")
public class PackageItem implements Serializable {

	public enum Status {
		SENT, DELIVERED, PENDING
	}

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private int idpackage;

	@Column(length = 45)
	private String description;

	@Column(nullable = false, length = 45)
	private String name;

	@Enumerated(EnumType.STRING)
	@Column(nullable = false, columnDefinition = "ENUM")
	private Status status;

	@Override
	public String toString() {
		return "PackageItem [idpackage=" + idpackage + ", description=" + description + ", name=" + name + ", status=" + status + ", tracking=" + tracking + ", destinationCity=" + destinationCity
				+ ", receiver=" + receiver + ", sender=" + sender + ", senderCity=" + senderCity + "]";
	}

	@Column(nullable = false, columnDefinition = "BIT")
	private boolean tracking;

	// bi-directional many-to-one association to City
	@ManyToOne
	@JoinColumn(name = "destinationCity", nullable = false)
	private City destinationCity;

	// bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name = "receiver", nullable = false)
	private User receiver;

	// bi-directional many-to-one association to User
	@ManyToOne
	@JoinColumn(name = "sender", nullable = false)
	private User sender;

	// bi-directional many-to-one association to City
	@ManyToOne
	@JoinColumn(name = "senderCity", nullable = false)
	private City senderCity;

	@OneToMany(fetch = FetchType.EAGER, targetEntity = RouteEntry.class)
	@JoinColumn(name = "package")
	private List<RouteEntry> route = new ArrayList<>();

	public int getIdpackage() {
		return idpackage;
	}

	public void setIdpackage(int idpackage) {
		this.idpackage = idpackage;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public boolean isTracking() {
		return tracking;
	}

	public void setTracking(boolean tracking) {
		this.tracking = tracking;
	}

	public City getDestinationCity() {
		return destinationCity;
	}

	public void setDestinationCity(City destinationCity) {
		this.destinationCity = destinationCity;
	}

	public User getReceiver() {
		return receiver;
	}

	public void setReceiver(User receiver) {
		this.receiver = receiver;
	}

	public User getSender() {
		return sender;
	}

	public void setSender(User sender) {
		this.sender = sender;
	}

	public City getSenderCity() {
		return senderCity;
	}

	public void setSenderCity(City senderCity) {
		this.senderCity = senderCity;
	}

	public PackageItem() {
		super();
		status = Status.PENDING;
		tracking = false;

	}

	public void addRouteEntry(RouteEntry entry) throws Exception {
		if (tracking) {
			route.add(entry);
		} else {
			throw new Exception("Tracking not enabled");
		}
	}

}