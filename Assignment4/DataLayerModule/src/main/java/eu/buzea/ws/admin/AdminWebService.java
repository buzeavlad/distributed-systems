package eu.buzea.ws.admin;

import java.util.Date;
import java.util.List;

import javax.jws.WebService;

import eu.buzea.dao.GenericDao;
import eu.buzea.dao.PackageDao;
import eu.buzea.domain.City;
import eu.buzea.domain.PackageItem;
import eu.buzea.domain.RouteEntry;
import eu.buzea.dto.PackageDto;

@WebService
public class AdminWebService {

	private PackageDao packageDao;

	public AdminWebService() {
		packageDao = new PackageDao();

	}

	public void createPackage(PackageDto dto) {
		packageDao.createPackage(dto);
	}

	public void deletePackage(String name) {
		packageDao.deletePackage(name);
	}

	public void enableTracking(String packageName) {
		packageDao.enableTracking(packageName);
	}

	public void setDelivered(String packageName) {
		packageDao.setDelivered(packageName);
	}

	public boolean addRouteEntry(String packageName, String cityName, Date when) {
		return packageDao.addRouteEntry(packageName, cityName, when);
	}

	public List<PackageItem> findAllPackages() {
		return packageDao.findAll();
	}

	public PackageItem findByName(String name) {
		return packageDao.findByName(name);
	}

	public List<RouteEntry> getRoute(String name) {
		return packageDao.getRoute(name);
	}

	public List<City> findAllCities() {
		GenericDao<City> genericDao = new GenericDao<>(City.class);
		return genericDao.findAll();
	}

}
