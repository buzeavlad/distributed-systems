package eu.buzea.dao;

import java.util.Date;
import java.util.List;

import javax.persistence.Query;
import javax.persistence.TypedQuery;

import eu.buzea.domain.City;
import eu.buzea.domain.PackageItem;
import eu.buzea.domain.PackageItem.Status;
import eu.buzea.domain.RouteEntry;
import eu.buzea.domain.User;
import eu.buzea.dto.PackageDto;

public class PackageDao extends GenericDao<PackageItem> {

	public PackageDao() {
		super(PackageItem.class);

	}

	public void createPackage(PackageDto dto) {
		entityManager.getTransaction().begin();
		try {
			User sender = findUser(dto.getSenderUsername());
			User receiver = findUser(dto.getReceiverUsername());
			City source = findCity(dto.getSenderCityName());
			City destination = findCity(dto.getDestinationCityName());

			PackageItem packageItem = new PackageItem();
			packageItem.setDescription(dto.getDescription());
			packageItem.setDestinationCity(destination);
			packageItem.setName(dto.getPackageName());
			packageItem.setReceiver(receiver);
			packageItem.setSender(sender);
			packageItem.setSenderCity(source);
			entityManager.persist(packageItem);
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			if (entityManager.getTransaction().isActive())
				entityManager.getTransaction().rollback();
		}

	}

	private City findCity(String senderCityName) {
		TypedQuery<City> query = entityManager.createQuery("Select city From City city where city.name='" + senderCityName + "'", City.class);
		return query.getSingleResult();
	}

	private User findUser(String username) {
		TypedQuery<User> query = entityManager.createQuery("Select user From User user where user.username = '" + username + "'", User.class);
		return query.getSingleResult();
	}

	public void deletePackage(String name) {
		entityManager.getTransaction().begin();
		Query query = entityManager.createQuery("DELETE FROM PackageItem p WHERE p.name = :name");
		query.setParameter("name", name).executeUpdate();
		entityManager.getTransaction().commit();
	}

	private PackageItem findPackage(String name) {
		TypedQuery<PackageItem> query = entityManager.createQuery("Select p From PackageItem p where p.name = '" + name + "'", PackageItem.class);
		return query.getSingleResult();
	}

	public void enableTracking(String packageName) {
		entityManager.getTransaction().begin();
		PackageItem packageItem = findPackage(packageName);
		packageItem.setTracking(true);
		entityManager.persist(packageItem);
		entityManager.getTransaction().commit();
	}

	public void setDelivered(String packageName) {
		entityManager.getTransaction().begin();
		PackageItem packageItem = findPackage(packageName);
		packageItem.setStatus(Status.DELIVERED);
		entityManager.persist(packageItem);
		entityManager.getTransaction().commit();
	}

	public boolean addRouteEntry(String packageName, String cityName, Date when) {
		entityManager.getTransaction().begin();
		PackageItem packageItem = findPackage(packageName);
		packageItem.setStatus(Status.SENT);
		City currentLocation = findCity(cityName);
		RouteEntry routeEntry = new RouteEntry();
		routeEntry.setCity(currentLocation);
		routeEntry.setPackageTracked(packageItem);
		routeEntry.setTime(when);
		try {
			packageItem.addRouteEntry(routeEntry);
			entityManager.persist(routeEntry);
			entityManager.persist(packageItem);
			entityManager.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (entityManager.getTransaction().isActive())
				entityManager.getTransaction().rollback();
			return false;
		}

		return true;
	}

	@Override
	public List<PackageItem> findAll() {
		entityManager.getTransaction().begin();
		TypedQuery<PackageItem> q = entityManager.createQuery("Select p from PackageItem p", PackageItem.class);
		List<PackageItem> resultList = q.getResultList();
		entityManager.getTransaction().commit();
		// List<PackageDto> dtoList = new ArrayList<>();
		// for (PackageItem item : resultList) {
		// PackageDto dto = new PackageDto(item.getSender().getUsername(),
		// item.getReceiver().getUsername(), item.getName(),
		// item.getDescription(), item.getSenderCity().getName(),
		// item.getDestinationCity().getName());
		// dtoList.add(dto);
		// }
		return resultList;
	}

	public PackageItem findByName(String name) {
		entityManager.getTransaction().begin();
		TypedQuery<PackageItem> query = entityManager.createQuery("Select p From PackageItem p where p.name = '" + name + "'", PackageItem.class);
		PackageItem item = query.getSingleResult();
		entityManager.getTransaction().commit();
		return item;
	}

	public List<RouteEntry> getRoute(String name) {
		entityManager.getTransaction().begin();
		TypedQuery<RouteEntry> q = entityManager.createQuery("Select r from RouteEntry r where r.packageTracked.name= :name", RouteEntry.class);
		q.setParameter("name", name);
		List<RouteEntry> result = q.getResultList();
		entityManager.getTransaction().commit();
		return result;
	}

}
