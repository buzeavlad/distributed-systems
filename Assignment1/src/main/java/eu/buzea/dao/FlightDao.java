package eu.buzea.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import eu.buzea.model.Flight;
import eu.buzea.util.HibernateUtil;

public class FlightDao {

	public FlightDao() {

	}

	public void create(Flight flight) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;

		try {
			tx = session.beginTransaction();
			int id = (Integer) session.save(flight);
			flight.setFlightNumber(id);
			tx.commit();

		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
	}

	@SuppressWarnings("unchecked")
	public List<Flight> findAll() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		List<Flight> list=null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Flight");
			list = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public Flight find(int flightNumber){
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		Flight flight = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Flight WHERE flightNumber = :id");
			query.setParameter("id", flightNumber);
			List<Flight> list = query.list();
			tx.commit();
			if (list != null && list.size() > 0) {
				flight = list.get(0);
			}
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return flight;
	}

	public void update(int idFlight, Flight newValues) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			Flight flight = (Flight) session.get(Flight.class, idFlight);
			flight.setAirplane(newValues.getAirplane());
			flight.setArrivalTime(newValues.getArrivalTime());
			flight.setBookings(newValues.getBookings());
			flight.setCityByArrivalCity(newValues.getCityByArrivalCity());
			flight.setCityByDepartureCity(newValues.getCityByDepartureCity());
			flight.setDepartureTime(newValues.getDepartureTime());
	
			session.update(flight);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	public void delete(int idFlight) {
		Flight flight = find(idFlight);

		if (flight != null) {
			Session session = HibernateUtil.getSessionFactory().openSession();
			Transaction tx = session.beginTransaction();
			session.delete(flight);
			tx.commit();
			session.close();
		}
	}

}
