package eu.buzea.servlets;

import java.io.IOException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import eu.buzea.constants.Constants;
import eu.buzea.entities.Car;
import eu.buzea.remotes.SellingServiceRemote;
import eu.buzea.remotes.TaxServiceRemote;

/**
 * Servlet implementation class CarCalculationServlet
 */
@WebServlet("/CarCalculationServlet")
public class CarCalculationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private TaxServiceRemote taxServiceRemote;
	private SellingServiceRemote sellingServiceRemote;
       
    @Override
	public void init() throws ServletException {
		super.init();
		Registry registry;
		try {
			registry = LocateRegistry.getRegistry("localhost", Constants.PORT_NUMBER);
			taxServiceRemote = (TaxServiceRemote) registry.lookup(Constants.TAX_SERVICE);
			sellingServiceRemote = (SellingServiceRemote) registry.lookup(Constants.SELLING_SERVICE);
		} catch (Exception e) {
			throw new ServletException(e);
		}
		
	}

	/**
     * @see HttpServlet#HttpServlet()
     */
    public CarCalculationServlet() {
        super();
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String capcityString = request.getParameter("engineCapacity");
		int capacity = Integer.parseInt(capcityString);
		
		String yearString = request.getParameter("year");
		int year = Integer.parseInt(yearString);
		
		String priceString = request.getParameter("price");
		double price = Double.parseDouble(priceString);
		
		Car car = new Car(year,capacity,price);
		String buttonPressed = request.getParameter("buttonPressed");
		if("Calculate Tax".equals(buttonPressed)){
			 double tax = taxServiceRemote.calculateTax(car);
			 request.setAttribute("tax", tax);
		}
		
		if("Calculate Selling Price".equals(buttonPressed)){
			
			double sellingPrice = sellingServiceRemote.calculateSellingPrice(car);
			request.setAttribute("sellingPrice", sellingPrice);
		}
		
		request.getRequestDispatcher("index.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
