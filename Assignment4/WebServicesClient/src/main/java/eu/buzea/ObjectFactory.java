
package eu.buzea;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the eu.buzea package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: eu.buzea
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetPackage }
     * 
     */
    public GetPackage createGetPackage() {
        return new GetPackage();
    }

    /**
     * Create an instance of {@link LoginResponse }
     * 
     */
    public LoginResponse createLoginResponse() {
        return new LoginResponse();
    }

    /**
     * Create an instance of {@link SearchPackage }
     * 
     */
    public SearchPackage createSearchPackage() {
        return new SearchPackage();
    }

    /**
     * Create an instance of {@link IsAdminResponse }
     * 
     */
    public IsAdminResponse createIsAdminResponse() {
        return new IsAdminResponse();
    }

    /**
     * Create an instance of {@link GetRouteResponse }
     * 
     */
    public GetRouteResponse createGetRouteResponse() {
        return new GetRouteResponse();
    }

    /**
     * Create an instance of {@link ArrayOfRouteEntryDTO }
     * 
     */
    public ArrayOfRouteEntryDTO createArrayOfRouteEntryDTO() {
        return new ArrayOfRouteEntryDTO();
    }

    /**
     * Create an instance of {@link IsAdmin }
     * 
     */
    public IsAdmin createIsAdmin() {
        return new IsAdmin();
    }

    /**
     * Create an instance of {@link SearchPackageResponse }
     * 
     */
    public SearchPackageResponse createSearchPackageResponse() {
        return new SearchPackageResponse();
    }

    /**
     * Create an instance of {@link ArrayOfPackageDTO }
     * 
     */
    public ArrayOfPackageDTO createArrayOfPackageDTO() {
        return new ArrayOfPackageDTO();
    }

    /**
     * Create an instance of {@link GetPackageResponse }
     * 
     */
    public GetPackageResponse createGetPackageResponse() {
        return new GetPackageResponse();
    }

    /**
     * Create an instance of {@link PackageDTO }
     * 
     */
    public PackageDTO createPackageDTO() {
        return new PackageDTO();
    }

    /**
     * Create an instance of {@link GetRoute }
     * 
     */
    public GetRoute createGetRoute() {
        return new GetRoute();
    }

    /**
     * Create an instance of {@link Login }
     * 
     */
    public Login createLogin() {
        return new Login();
    }

    /**
     * Create an instance of {@link HelloWorld }
     * 
     */
    public HelloWorld createHelloWorld() {
        return new HelloWorld();
    }

    /**
     * Create an instance of {@link ConfirmDeliveryResponse }
     * 
     */
    public ConfirmDeliveryResponse createConfirmDeliveryResponse() {
        return new ConfirmDeliveryResponse();
    }

    /**
     * Create an instance of {@link RegisterResponse }
     * 
     */
    public RegisterResponse createRegisterResponse() {
        return new RegisterResponse();
    }

    /**
     * Create an instance of {@link ListAllPackagesResponse }
     * 
     */
    public ListAllPackagesResponse createListAllPackagesResponse() {
        return new ListAllPackagesResponse();
    }

    /**
     * Create an instance of {@link ConfirmDelivery }
     * 
     */
    public ConfirmDelivery createConfirmDelivery() {
        return new ConfirmDelivery();
    }

    /**
     * Create an instance of {@link ListAllPackages }
     * 
     */
    public ListAllPackages createListAllPackages() {
        return new ListAllPackages();
    }

    /**
     * Create an instance of {@link HelloWorldResponse }
     * 
     */
    public HelloWorldResponse createHelloWorldResponse() {
        return new HelloWorldResponse();
    }

    /**
     * Create an instance of {@link Register }
     * 
     */
    public Register createRegister() {
        return new Register();
    }

    /**
     * Create an instance of {@link RouteEntryDTO }
     * 
     */
    public RouteEntryDTO createRouteEntryDTO() {
        return new RouteEntryDTO();
    }

}
