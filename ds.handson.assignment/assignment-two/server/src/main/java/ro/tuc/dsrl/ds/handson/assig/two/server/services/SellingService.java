package ro.tuc.dsrl.ds.handson.assig.two.server.services;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.ISellingService;

public class SellingService implements ISellingService {



	@Override
	public double calculateSellingPrice(Car car) {
		double purchasingPrice = car.getPrice();
		int carYear = car.getYear();
		double sellingPrice = purchasingPrice - (purchasingPrice/7)*(2015-carYear);
		return sellingPrice;
	}

}
