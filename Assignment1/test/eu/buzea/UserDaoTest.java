package eu.buzea;

import org.junit.Test;

import eu.buzea.dao.UserDao;
import eu.buzea.model.User;


public class UserDaoTest {

	@Test
	public void testInsert(){
		UserDao dao=new UserDao();
		User randomUser = new User("vlad", "paa");
		dao.create(randomUser);
	}
	
	@Test
	public void testDelete(){
		UserDao dao = new UserDao();
		User user=dao.deleteUser("vlad");
		System.out.println(user.getUsername());
	}
	

}
