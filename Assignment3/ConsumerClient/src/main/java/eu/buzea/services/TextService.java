package eu.buzea.services;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

import eu.buzea.dvd.Dvd;

public class TextService {

	public static final String BASE_FOLDER = "./DVDs";

	public TextService() {
	}

	public boolean writeDvdToFile(Dvd dvd) {
		File msgFile = new File(BASE_FOLDER, "DVD" + dvd.getArtist() + "_" + dvd.getAlbumName() + ".txt");
		msgFile.getParentFile().mkdirs();
		String dvdString = "DVD:\n\t" + "Artist: " + dvd.getArtist() + "\n\tAlbum: " + dvd.getAlbumName() + "\n\tPrice: " + dvd.getPrice() + "$\n\tYear: " + dvd.getYear() + "\n";
		PrintWriter printWriter = null;
		try {
			printWriter = new PrintWriter(msgFile);
			printWriter.write(dvdString);
			return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (printWriter != null)
				printWriter.close();
		}
		return false;
	}

}
