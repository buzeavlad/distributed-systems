<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html>
<head>
<title>Airport admin</title>
<link rel="stylesheet" href="css/bootstrap.css" />
<link rel="stylesheet" href="css/bootstrap-theme.css" />
<SCRIPT type="text/javascript">
	window.onload = function() {
		var msg = '${message}';
		if (msg != null && msg != "") {
			alert(msg);
		}

	}
</SCRIPT>
</head>
<body>
	<%
		boolean authorized = false;
		Object admin = request.getSession().getAttribute("admin");
		if(admin!=null){
			authorized=true;
		}
		
		if (!authorized) {
			response.sendRedirect("index.jsp");
		}
	%>
	<div id="myHeader" align="right">
		<form action="LoginServlet">
			<input name="logOut" value="Log Out" type="submit" />
		</form>
	</div>
	<form action="AdminServlet" method="post">

		<div align="center" style="padding-bottom: 70px;">
			<h1>Welcome to admin control panel</h1>

			<div>
				<table border="1">
					<tr>
						<th>Number</th>
						<th>Airplane type</th>
						<th>Departure city</th>
						<th>Departure time(UTC)</th>
						<th>Arrival city</th>
						<th>Arrival time(UTC)</th>
						<th></th>
					</tr>
					<c:forEach var="flight" items="${flights}">
						<tr>
							<td><c:out value="${flight.flightNumber}"></c:out></td>
							<td><c:out value="${flight.airplane.type }"></c:out></td>
							<td><c:out value="${flight.cityByDepartureCity.name }"></c:out></td>
							<td><c:out value="${flight.departureTime }"></c:out></td>
							<td><c:out value="${flight.cityByArrivalCity.name }"></c:out></td>
							<td><c:out value="${flight.arrivalTime }"></c:out></td>

							<td><input type="radio" name="flightNumber"
								value="${flight.flightNumber }" checked="checked" /></td>

						</tr>

					</c:forEach>
				</table>
			</div>


		</div>
		<DIV id="footer" align="center">
			<table border="1">
				<tr>
					<td><input name="buttonPressed" type="submit"
						value="Create new flight" /></td>
					<td><input type="submit" name="buttonPressed"
						value="Update flight" /></td>
					<td><input type="submit" name="buttonPressed"
						value="Delete flight" /></td>
				</tr>
				<tr>
					<td><input name="buttonPressed" type="submit"
						value="Create new booking" /></td>
					<td><label>User ID:</label></td>
					<td><input type="text" name="username" pattern=".{3,20}" /></td>

				</tr>

			</table>
		</DIV>
	</form>
</body>
</html>
