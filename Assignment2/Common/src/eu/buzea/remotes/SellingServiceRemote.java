package eu.buzea.remotes;

import java.rmi.Remote;
import java.rmi.RemoteException;

import eu.buzea.entities.Car;

public interface SellingServiceRemote extends Remote {

	public double calculateSellingPrice(Car car) throws RemoteException;
}
