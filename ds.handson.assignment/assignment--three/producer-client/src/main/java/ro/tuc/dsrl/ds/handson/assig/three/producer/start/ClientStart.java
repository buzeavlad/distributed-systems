package ro.tuc.dsrl.ds.handson.assig.three.producer.start;

import java.io.IOException;

import ro.tuc.dsrl.ds.handson.assig.three.entities.Dvd;
import ro.tuc.dsrl.ds.handson.assig.three.producer.connection.QueueServerConnection;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description:
 *	Starting point for the Producer Client application. This
 *	application will send several messages to be inserted
 *  in the queue server (i.e. to be sent via email by a consumer).
 */
public class ClientStart {
	private static final String HOST = "localhost";
	private static final int PORT = 8888;

	private ClientStart() {
	}

	public static void main(String[] args) {
		QueueServerConnection queue = new QueueServerConnection(HOST, PORT);

		try {
				Dvd myDvd= new Dvd("Roxette",1990,10.0);
				queue.writeMessage(Dvd.serialize(myDvd));
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
