<html>
<head>
<title>DS DVD Store</title>
<LINK rel="stylesheet" href="css/bootstrap.css" />
<LINK rel="stylesheet" href="css/bootstrap-theme.css" />
</head>
<body>
	<div align="center">
		<h2>Add new DVD to Collection</h2>
		<form action="postDvd" method="get">
			<table border="1">
				<tr>
					<td>Artist</td>
					<td><input type="text" name="artist" /></td>
				</tr>
				<tr>
					<td>Album Name</td>
					<td><input type="text" name="album" /></td>
				</tr>
				<tr>
					<td>Year</td>
					<td><input type="number" name="year" value="2010" min="1900" max="2015" /></td>
				</tr>
				<tr>
					<td>Price</td>
					<td><input type="number" name="price" value ="10" min="0.1" step="0.01" />$</td>
				</tr>
				<tr>
					<td colspan="2"><input type="submit" value="Post DVD" /></td>
				</tr>
			</table>
		</form>
		
	</div>
</body>
</html>
