﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;

namespace ClientWebService
{
    /// <summary>
    /// Summary description for ClientWs
    /// </summary>
    // [WebService(Namespace = "http://tempuri.org/")]
    [WebService(Namespace = "http://buzea.eu/",Name ="ClientWs",Description ="Client WebService")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class ClientWs : System.Web.Services.WebService
    {
        private MySqlConnection connection;

        public ClientWs()
        {
           String server = "localhost";
            String database = "tracking-system";
            String uid = "root";
            String password = "root";
            String  connectionString;
            connectionString = "SERVER=" + server + ";" + "DATABASE=" +
            database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";
            connection = new MySqlConnection(connectionString);
        }

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        public bool login(String username,String password)
        {

            string query = "SELECT Count(*) FROM user where username = '"+username+"' and password = '"+password+"'";
            int Count = -1;

            //Open Connection
            if (this.OpenConnection() == true)
            {
                //Create Mysql Command
                MySqlCommand cmd = new MySqlCommand(query, connection);

                //ExecuteScalar will return one value
                Count = int.Parse(cmd.ExecuteScalar() + "");

                //close Connection
                this.CloseConnection();

                if (Count == 1)
                {
                    return true;
                }
            }
           
            return false;
        }

        [WebMethod]
        public bool isAdmin(String username)
        {

            string query = "SELECT Count(*) FROM user where username = '" + username + "' and isAdmin = 1";
            int Count = -1;

            //Open Connection
            if (this.OpenConnection() == true)
            {
                //Create Mysql Command
                MySqlCommand cmd = new MySqlCommand(query, connection);

                //ExecuteScalar will return one value
                Count = int.Parse(cmd.ExecuteScalar() + "");

                //close Connection
                this.CloseConnection();

                if (Count == 1)
                {
                    return true;
                }
            }

            return false;
             
        }

        [WebMethod]
        public bool register(String username,String password)
        {

            string query = "INSERT INTO user (username, password) VALUES('"+username+"', '"+password+"')";

            try {
                //open connection
                if (this.OpenConnection() == true)
                {
                    //create command and assign the query and connection from the constructor
                    MySqlCommand cmd = new MySqlCommand(query, connection);

                    //Execute command
                    cmd.ExecuteNonQuery();

                    //close connection
                    this.CloseConnection();
                    return true;
                }
            }catch(Exception e)
            {

            }
            return false;
        }

        [WebMethod]
        public bool confirmDelivery(String packageName)
        {
            string query = "UPDATE package SET `status`='DELIVERED' WHERE `name`='"+packageName+"';";

            try
            {
                //open connection
                if (this.OpenConnection() == true)
                {
                    //create command and assign the query and connection from the constructor
                    MySqlCommand cmd = new MySqlCommand(query, connection);

                    //Execute command
                    cmd.ExecuteNonQuery();

                    //close connection
                    this.CloseConnection();
                    return true;
                }
            }
            catch (Exception e)
            {

            }
            return false;
        }


        [WebMethod]
        public List<PackageDTO> listAllPackages(String username)
        {
            string query = "SELECT idpackage, package.name, description, status, tracking, sender.username as senderUsername, recv.username as receiverUsername, senderC.Name as senderCity,destination.Name as destinationCity FROM package join user sender on package.sender=sender.idUser join user recv on package.receiver=recv.idUser join city senderC on senderCity=senderC.idCity join city destination on destinationCity=destination.idCity where sender.username = '"+username+"' or recv.username ='"+username+"'";
            List<PackageDTO> resultList = new List<PackageDTO>();

            if (this.OpenConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    PackageDTO item = new PackageDTO();
                    item.Id = int.Parse(dataReader["idPackage"] + "");
                    int YesNo = int.Parse(dataReader["tracking"] + "");
                    item.Tracking = (YesNo == 1);
                    item.Name = dataReader["name"] + "";
                    item.Status = dataReader["status"] + "";
                    item.Description = dataReader["description"] + "";
                    item.SenderUsername = dataReader["senderUsername"] + "";

                    item.ReceiverUsername = dataReader["receiverUsername"] + "";

                    item.SenderCity = dataReader["senderCity"] + "";


                    item.DestinationCity = dataReader["destinationCity"] + "";
                    resultList.Add(item);

                }

                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();

                //return list to be displayed
                return resultList;
            }
            return null;
        }

        [WebMethod]
        public List<PackageDTO> searchPackage(String packageName,String username)
        {
            string query= "SELECT idpackage, package.name, description, status, tracking, sender.username as senderUsername, recv.username as receiverUsername, senderC.Name as senderCity,destination.Name as destinationCity FROM package join user sender on package.sender=sender.idUser join user recv on package.receiver=recv.idUser join city senderC on senderCity=senderC.idCity join city destination on destinationCity=destination.idCity where ( package.name LIKE '%" + packageName+ "%' or package.description LIKE '%" + packageName + "%' ) and (sender.username = '" + username+"' or recv.username = '"+username+"')" ;
            List<PackageDTO> resultList = new List<PackageDTO>();

            if (this.OpenConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    PackageDTO item = new PackageDTO();
                    item.Id= int.Parse(dataReader["idPackage"] + "");
                    int YesNo = int.Parse(dataReader["tracking"] + "");
                    item.Tracking = (YesNo == 1);
                    item.Name = dataReader["name"] + "";
                    item.Status = dataReader["status"] + "";
                    item.Description = dataReader["description"] + "";
                    item.SenderUsername = dataReader["senderUsername"] + "";
                                        
                    item.ReceiverUsername = dataReader["receiverUsername"] + "";

                    item.SenderCity = dataReader["senderCity"] + "";


                    item.DestinationCity = dataReader["destinationCity"] + "";
                    resultList.Add(item);

                }

                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();

                //return list to be displayed
                return resultList;
            }
            return null;
        }

        [WebMethod]
        public PackageDTO getPackage(String packageName)
        {
            string query = "SELECT idpackage, package.name, description, status, tracking, sender.username as senderUsername, recv.username as receiverUsername, senderC.Name as senderCity,destination.Name as destinationCity FROM package join user sender on package.sender=sender.idUser join user recv on package.receiver=recv.idUser join city senderC on senderCity=senderC.idCity join city destination on destinationCity=destination.idCity where package.name = '" + packageName + "'";
            PackageDTO item = null;

            if (this.OpenConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                //Read the data and store them in the list
                if (dataReader.Read())
                {
                   item = new PackageDTO();
                    item.Id = int.Parse(dataReader["idPackage"] + "");
                    int YesNo = int.Parse(dataReader["tracking"] + "");
                    item.Tracking = (YesNo == 1);
                    item.Name = dataReader["name"] + "";
                    item.Status = dataReader["status"] + "";
                    item.Description = dataReader["description"] + "";
                    item.SenderUsername = dataReader["senderUsername"] + "";

                    item.ReceiverUsername = dataReader["receiverUsername"] + "";

                    item.SenderCity = dataReader["senderCity"] + "";


                    item.DestinationCity = dataReader["destinationCity"] + "";
                   

                }

                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();
                return item;

            }
            return item;
        }

        [WebMethod]
        public List<RouteEntryDTO> getRoute(String packageName)
        {
            List<RouteEntryDTO> resultList = null;
            String query = "SELECT package.name as name, time, city.name as location FROM routeentry join package on routeentry.package=package.idpackage join city on location=idCity where package.name ='"+packageName+"'";
            if (this.OpenConnection() == true)
            {
                //Create Command
                MySqlCommand cmd = new MySqlCommand(query, connection);
                //Create a data reader and Execute the command
                MySqlDataReader dataReader = cmd.ExecuteReader();

                resultList = new List<RouteEntryDTO>();
                //Read the data and store them in the list
                while (dataReader.Read())
                {
                    RouteEntryDTO item = new RouteEntryDTO();
                   
                    item.PackageName = dataReader["name"] + "";
                    item.Time = dataReader["time"] + "";
                    item.Location = dataReader["location"] + "";
                    resultList.Add(item);

                }

                //close Data Reader
                dataReader.Close();

                //close Connection
                this.CloseConnection();

                //return list to be displayed
                return resultList;
            }
            
            return resultList;
        }

        private bool OpenConnection()
        {
            try
            {
                connection.Open();
                return true;
            }
            catch (MySqlException ex)
            {
                //When handling errors, you can your application's response based 
                //on the error number.
                //The two most common error numbers when connecting are as follows:
                //0: Cannot connect to server.
                //1045: Invalid user name and/or password.
                switch (ex.Number)
                {
                    case 0:
                       Console.Out.Write("Cannot connect to server.  Contact administrator");
                        break;

                    case 1045:
                        Console.Out.Write("Invalid username/password, please try again");
                        break;
                }
                return false;
            }
        }

        //Close connection
        private bool CloseConnection()
        {
            try
            {
                connection.Close();
                return true;
            }
            catch (MySqlException ex)
            {
                Console.Out.Write(ex.Message);
                return false;
            }
        }
    }
}
