<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="css/bootstrap.css" />
<link rel="stylesheet" href="css/bootstrap-theme.css" />
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Tracking System</title>
</head>
<body>
	<div id="myHeader" align="right">
		<form action="logIn">
			<input name="logOut" value="Log Out" type="submit" />
		</form>
		<form action="ClientActions" method="post">
			<input name="home" value="Home" type="submit">
		</form>
	</div>
	<div align="center">
		<H1>Entry List</H1>
		<table border="1">
			<tr>
				<TH>City</TH>
				<TH>Time</TH>
			</tr>
			<c:forEach var="r" items="${route}">
				<tr>
					<td>${r.location}</td>
					<td>${r.time}</td>
				</tr>
			</c:forEach>
		</table>
	</div>
</body>
</html>