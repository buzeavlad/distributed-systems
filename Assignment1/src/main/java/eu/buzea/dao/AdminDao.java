package eu.buzea.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import eu.buzea.model.Admin;
import eu.buzea.util.HibernateUtil;

public class AdminDao {

	public AdminDao() {

	}

	public void create(Admin admin) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		UserDao userDao = new UserDao();
		try {
			if (userDao.findUser(admin.getUsername()) == null) {
				tx = session.beginTransaction();
				int userId = (Integer) session.save(admin);
				admin.setIdadmin(userId);
				tx.commit();
			}
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}

	}

	@SuppressWarnings("unchecked")
	public Admin find(String username) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		Admin admin = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Admin WHERE username = :username");
			query.setParameter("username", username);
			List<Admin> list = query.list();
			tx.commit();
			if (list != null && list.size() > 0) {
				admin = list.get(0);
			}
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return admin;
	}

}
