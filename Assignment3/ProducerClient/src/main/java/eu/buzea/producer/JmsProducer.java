package eu.buzea.producer;

import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;

import eu.buzea.dvd.Dvd;
import eu.buzea.messaging.JmsEndPoint;

public class JmsProducer extends JmsEndPoint {

	private MessageProducer myMsgProducer;

	public JmsProducer(String queueName) {
		super(queueName);
		 try {
			myMsgProducer = mySess.createProducer(myQueue);
		} catch (JMSException e) {
			e.printStackTrace();
		}
	}
	
	public boolean sendDvd(Dvd dvd) {
		try {
			ObjectMessage message = mySess.createObjectMessage(dvd);
			myMsgProducer.send(message);
			return true;
		} catch (JMSException e) {
			e.printStackTrace();
		}

		return false;
	}

}
