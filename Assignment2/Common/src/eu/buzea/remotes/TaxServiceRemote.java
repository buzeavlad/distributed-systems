package eu.buzea.remotes;

import java.rmi.Remote;
import java.rmi.RemoteException;

import eu.buzea.entities.Car;

public interface TaxServiceRemote extends Remote {
	
	public double calculateTax(Car car) throws RemoteException;
}
