package eu.buzea.test;

import java.util.Date;

import org.junit.Test;

import eu.buzea.dao.GenericDao;
import eu.buzea.dao.PackageDao;
import eu.buzea.domain.City;
import eu.buzea.dto.PackageDto;

public class DaoTest {

	@Test
	public void testFindCity() {
		GenericDao<City> cityDao = new GenericDao<>(City.class);
		System.out.println(cityDao.find(1));
	}

	@Test
	public void testPackage() {
		PackageDao packageDao = new PackageDao();
		packageDao.deletePackage("test package");
		PackageDto dto = new PackageDto("dsTest", "receiverTest", "test package", "description", "Cluj-Napoca", "Bucuresti");
		packageDao.createPackage(dto);
		// packageDao.enableTracking("newPackage");
		// packageDao.addRouteEntry("newPackage", "Bucuresti", new Date());
	}

	// @Test
	public void testAddRouteEntry() {
		PackageDao packageDao = new PackageDao();
		// packageDao.deletePackage("newPackage");
		// PackageDto dto = new PackageDto("dsTest", "receiverTest",
		// "newPackage", "", "Cluj-Napoca", "Bucuresti");
		packageDao.enableTracking("newPackage");
		packageDao.addRouteEntry("newPackage", "Bucuresti", new Date());
	}

}
