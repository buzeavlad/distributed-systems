package eu.buzea.servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import eu.buzea.dao.FlightDao;
import eu.buzea.model.City;
import eu.buzea.model.Flight;

/**
 * Servlet implementation class QueryLocalTimeServlet
 */
@WebServlet("/QueryLocalTimeServlet")
public class QueryLocalTimeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public QueryLocalTimeServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			int flightNb = Integer.parseInt(request.getParameter("flightNb"));
			FlightDao flightDao = new FlightDao();
			Flight flight = flightDao.find(flightNb);
			String departTime = getLocalTime(flight.getCityByDepartureCity(), flight.getDepartureTime());
			String arrivalTime = getLocalTime(flight.getCityByArrivalCity(), flight.getArrivalTime());

			request.setAttribute("depart", departTime);
			request.setAttribute("arrival", arrivalTime);

		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		request.getRequestDispatcher("localTime.jsp").forward(request, response);
	}

	private String getLocalTime(City city, Date date) throws UnirestException, ParseException {
		String latitude = city.getLatitude();
		String longitude = city.getLongitude();
		HttpResponse<JsonNode> responseJson = Unirest
				.get("https://worldtimeiodeveloper.p.mashape.com/geo?latitude=" + latitude + "&longitude=" + longitude)
				.header("X-Mashape-Key", "E7urukwjk5msh7UiD1vwrfggyi6Vp16G4yyjsnHAb33FybB8iS")
				.header("Accept", "application/json").asJson();
		JsonNode jsonNode = responseJson.getBody();
		JSONObject body = jsonNode.getObject();
		JSONObject summary = body.getJSONObject("summary");
		String utc = summary.getString("utc");
		String localTime = summary.getString("local");
		System.out.println(utc+"\n"+localTime);
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date utcDate = df.parse(utc);
		Date localDate = df.parse(localTime);
		System.out.println("Parsed:\n"+df.format(utcDate));
		System.out.println(df.format(localDate)+"\n");
		Date result = new Date();
		result.setTime(date.getTime() + (localDate.getTime() - utcDate.getTime()));
		return df.format(result);
	}

}
