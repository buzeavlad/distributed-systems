package eu.buzea;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import eu.buzea.ws.admin.AdminWebService;
import eu.buzea.ws.admin.AdminWebServiceService;
import eu.buzea.ws.admin.City;
import eu.buzea.ws.admin.PackageDto;
import eu.buzea.ws.admin.PackageItem;
import eu.buzea.ws.admin.RouteEntry;

/**
 * Servlet implementation class AdminActions
 */
@WebServlet("/AdminActions")
public class AdminActions extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdminActions() {
		super();

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		AdminWebService adminWs = new AdminWebServiceService().getAdminWebServicePort();
		PrintWriter out = response.getWriter();
		String name = request.getParameter("pkgName");
		String enableTracking = request.getParameter("enable");
		if (enableTracking != null) {
			adminWs.enableTracking(name);
		}

		String delete = request.getParameter("delete");
		if (delete != null) {
			adminWs.deletePackage(name);
		}

		String addPackage = request.getParameter("addPackage");
		if (addPackage != null) {
			PackageDto dto = new PackageDto();
			dto.setDescription(request.getParameter("description"));
			dto.setDestinationCityName(request.getParameter("to"));
			dto.setPackageName(name);
			dto.setReceiverUsername(request.getParameter("receiver"));
			dto.setSenderCityName(request.getParameter("from"));
			dto.setSenderUsername(request.getParameter("sender"));
			adminWs.createPackage(dto);
			out.println("<script type=\"text/javascript\">");
			out.println("alert('Package created created');");
			out.println("</script>");
		}

		String add = request.getParameter("add");
		String submitEntry = request.getParameter("submitEntry");
		try {
			if (submitEntry != null) {
				String city = request.getParameter("city");
				String date = request.getParameter("time");
				String pkgName = request.getParameter("pkgName");
				Date javaDate = stringToJavaDate(date);
				GregorianCalendar gregorianCalendar = (GregorianCalendar) GregorianCalendar.getInstance();
				gregorianCalendar.setTime(javaDate);
				XMLGregorianCalendar result = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
				adminWs.addRouteEntry(pkgName, city, result);
				name = pkgName;
				add = "true";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (add != null) {

			List<City> cities = adminWs.findAllCities();

			request.setAttribute("cities", cities);
			if (add.equals("Add new package")) {
				request.getRequestDispatcher("./addPackage.jsp").forward(request, response);
			} else {
				PackageItem item = adminWs.findByName(name);
				List<RouteEntry> route = adminWs.getRoute(name);
				request.setAttribute("item", item);
				request.setAttribute("route", route);
				request.getRequestDispatcher("./addEntry.jsp").forward(request, response);
			}
		} else {

			List<PackageItem> packageList = adminWs.findAllPackages();
			request.setAttribute("packageList", packageList);
			request.getRequestDispatcher("./admin.jsp").forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		doGet(request, response);
	}

	private static Date stringToJavaDate(String sDate) throws ParseException {
		Date date = null;
		sDate = sDate.replace("T", " ");
		date = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(sDate);
		return date;
	}

}
