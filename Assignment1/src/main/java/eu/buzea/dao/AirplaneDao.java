package eu.buzea.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import eu.buzea.model.Airplane;
import eu.buzea.util.HibernateUtil;

public class AirplaneDao {

	public AirplaneDao() {
	}
	
	@SuppressWarnings("unchecked")
	public List<Airplane> findAll() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		List<Airplane> list=null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Airplane");
			list = query.list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public Airplane find(int id){
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		Airplane plane = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM Airplane WHERE idairplane = :id");
			query.setParameter("id", id);
			List<Airplane> list = query.list();
			tx.commit();
			if (list != null && list.size() > 0) {
				plane = list.get(0);
			}
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return plane;
	}

}
