<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Flight CRUD</title>
<link rel="stylesheet" href="css/bootstrap.css" />
<link rel="stylesheet" href="css/bootstrap-theme.css" />
<script src="javascript/validateFlight.js"></script>
</head>
<body>
	<%
		boolean authorized = false;
		Object admin = request.getSession().getAttribute("admin");
		if (admin != null) {
			authorized = true;
		}

		if (!authorized) {
			response.sendRedirect("index.jsp");
		}
	%>

	<div align="center">
		<h1>Flight Data</h1>
		<form action="FlightsServlet" method="post" name="FlightForm"
			onsubmit="return validateForm()">
			<table border="1">
				<tr>
					<td>Airplane Type</td>
					<td><select name="airplaneTypeSelectList">
							<c:forEach var="airplaneIterator" items="${airplanes}">
								<c:choose>
									<c:when
										test="${airplaneIterator.idairplane == flight.airplane.idairplane }">
										<option value="${airplaneIterator.idairplane }"
											selected="selected">${airplaneIterator.type }</option>
									</c:when>
									<c:otherwise>
										<option value="${airplaneIterator.idairplane }">${airplaneIterator.type }</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
					</select></td>
				</tr>
				<tr>
					<td>Departure City</td>
					<td><select name="departCitySelectList">
							<c:forEach var="cityIterator" items="${cities}">
								<c:choose>
									<c:when
										test="${cityIterator.idcity == flight.cityByDepartureCity.idcity }">
										<option value="${cityIterator.idcity }" selected="selected">${cityIterator.name }</option>
									</c:when>
									<c:otherwise>
										<option value="${cityIterator.idcity }">${cityIterator.name }</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
					</select></td>
				</tr>
				<tr>
					<td>Departure time</td>
					<td><input type="datetime-local" name="departTime"
						value="${departTime }" /></td>
				</tr>
				<tr>
					<td>Arrival City</td>
					<td><select name="arivalCitySelectList">
							<c:forEach var="cityIterator" items="${cities}">
								<c:choose>
									<c:when
										test="${cityIterator.idcity == flight.cityByArrivalCity.idcity }">
										<option value="${cityIterator.idcity }" selected="selected">${cityIterator.name }</option>
									</c:when>
									<c:otherwise>
										<option value="${cityIterator.idcity }">${cityIterator.name }</option>
									</c:otherwise>
								</c:choose>
							</c:forEach>
					</select></td>
				</tr>
				<tr>
					<td>Arrival Time</td>
					<td><input type="datetime-local" name="arrivalTime"
						value="${arrivalTime}" /></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><input type="submit"
						name="submit" value="${buttonPressed}" /> <INPUT type="hidden"
						value="${flight.flightNumber}" name="flightNumber" /></td>
				</tr>
			</table>
		</form>
	</div>


</body>
</html>