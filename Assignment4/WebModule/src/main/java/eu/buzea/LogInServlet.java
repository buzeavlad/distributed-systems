package eu.buzea;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import eu.buzea.ws.admin.AdminWebService;
import eu.buzea.ws.admin.AdminWebServiceService;
import eu.buzea.ws.admin.PackageItem;

/**
 * Servlet implementation class LogInServlet
 */
@WebServlet({ "/LogInServlet", "/logIn" })
public class LogInServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LogInServlet() {
		super();

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String logOut = request.getParameter("logOut");
		String register = request.getParameter("register");
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		PrintWriter out = response.getWriter();
		ClientWsSoap clientWs = new ClientWs().getClientWsSoap();
		if (register != null) {
			if (clientWs.register(username, password)) {
				out.println("<script type=\"text/javascript\">");
				out.println("alert('Account created');");
				out.println("location='index.jsp';");
				out.println("</script>");
			} else {
				out.println("<script type=\"text/javascript\">");
				out.println("alert('Username already exists');");
				out.println("location='register.jsp';");
				out.println("</script>");
			}

		} else if (logOut != null) {
			request.getSession().invalidate();
			response.sendRedirect(".");
		} else if (clientWs.login(username, password)) {
			if (clientWs.isAdmin(username)) {
				request.getSession().setAttribute("admin", username);
				AdminWebService adminWs = new AdminWebServiceService().getAdminWebServicePort();
				List<PackageItem> packageList = adminWs.findAllPackages();
				request.setAttribute("packageList", packageList);
				request.getRequestDispatcher("./admin.jsp").forward(request, response);
			} else {
				request.getSession().setAttribute("user", username);
				List<PackageDTO> packages = clientWs.listAllPackages(username).getPackageDTO();
				request.setAttribute("packageList", packages);
				request.getRequestDispatcher("./client.jsp").forward(request, response);
			}
		} else {
			out.println("<script type=\"text/javascript\">");
			out.println("alert('Invalid credentials');");
			out.println("location='index.jsp';");
			out.println("</script>");
			// response.sendRedirect(".");
		}

	}

}
