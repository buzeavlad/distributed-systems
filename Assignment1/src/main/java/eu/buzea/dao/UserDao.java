package eu.buzea.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import eu.buzea.model.User;
import eu.buzea.util.HibernateUtil;

public class UserDao {

	public UserDao() {
	}

	public User create(User user) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		AdminDao adminDao = new AdminDao();
		try {
			if (adminDao.find(user.getUsername()) == null) {
				tx = session.beginTransaction();
				int userId = (Integer) session.save(user);
				user.setUser(userId);
				tx.commit();
			}
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return user;
	}

	@SuppressWarnings("unchecked")
	public User findUser(int id) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		User user = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM User WHERE id = :id");
			query.setParameter("id", id);
			List<User> list = query.list();
			tx.commit();
			if (list != null && list.size() > 0) {
				user = list.get(0);
			}
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return user;
	}

	@SuppressWarnings("unchecked")
	public User findUser(String username) {

		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		User user = null;
		try {
			tx = session.beginTransaction();
			Query query = session.createQuery("FROM User WHERE username = :username");
			query.setParameter("username", username);
			List<User> list = query.list();
			tx.commit();
			if (list != null && list.size() > 0) {
				user = list.get(0);
			}
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return user;
	}

	public User deleteUser(int id) {

		User user = findUser(id);

		if (user != null) {
			Session session = HibernateUtil.getSessionFactory().openSession();
			Transaction tx = session.beginTransaction();
			session.delete(user);
			tx.commit();
			session.close();
		}

		return user;
	}

	public User deleteUser(String username) {

		User user = findUser(username);

		if (user != null) {
			Session session = HibernateUtil.getSessionFactory().openSession();
			Transaction tx = session.beginTransaction();
			session.delete(user);
			tx.commit();
			session.close();
		}

		return user;
	}

	public void updateUser(int id, User newValues) {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		try {
			tx = session.beginTransaction();
			User user = (User) session.get(User.class, id);
			user.setUsername(newValues.getUsername());
			user.setPassword(newValues.getPassword());
			session.update(user);
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	@SuppressWarnings("unchecked")
	public List<User> findAll() {
		Session session = HibernateUtil.getSessionFactory().openSession();
		Transaction tx = null;
		List<User> users = null;
		try {
			tx = session.beginTransaction();
			users = session.createQuery("FROM User").list();
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null) {
				tx.rollback();
			}
		} finally {
			session.close();
		}
		return users;
	}

}
