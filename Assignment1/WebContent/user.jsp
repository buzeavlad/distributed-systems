<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="x" uri="http://java.sun.com/jsp/jstl/xml"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page import="eu.buzea.model.*"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Airport user page</title>
<link rel="stylesheet" href="css/bootstrap.css" />
<link rel="stylesheet" href="css/bootstrap-theme.css" />
</head>
<body>
	<%
		boolean authorized = false;
		String username = "";

		User user = (User) request.getSession().getAttribute("user");
		if (user != null) {
			username = user.getUsername();
			authorized = true;
		}

		if (!authorized) {
			response.sendRedirect("index.jsp");
		}
	%>
	<div id="myHeader" align="right">
		<form action="LoginServlet">
			<input name="logOut" value="Log Out" type="submit" />
		</form>
	</div>
	<center>
		<h1>
			Welcome
			<%=username%></h1>
		<form action="QueryLocalTimeServlet" method="post">
			<table border="2px">
				<tr>
					<th colspan="7" title="Future and past flights">Flight list</th>
				</tr>
				<tr>
					<th>Number</th>
					<th>Airplane type</th>
					<th>Departure city</th>
					<th>Departure time(UTC)</th>
					<th>Arrival city</th>
					<th>Arrival time(UTC)</th>
					<th>Options</th>
				</tr>
				<c:forEach var="booking" items="${bookings}">
					<tr>
						<td><c:out value="${booking.flight.flightNumber}"></c:out></td>
						<td><c:out value="${booking.flight.airplane.type }"></c:out></td>
						<td><c:out
								value="${booking.flight.cityByDepartureCity.name }"></c:out></td>
						<td><c:out value="${booking.flight.departureTime }"></c:out></td>
						<td><c:out value="${booking.flight.cityByArrivalCity.name }"></c:out></td>
						<td><c:out value="${booking.flight.arrivalTime }"></c:out></td>

						<td><input type="radio" name="flightNb"
							value="${booking.flight.flightNumber}" checked="checked" /></td>

					</tr>
				</c:forEach>
				<tr align="right">
					<td><input type="submit" value="Query local time" /></td>
				</tr>
			</table>
		</form>
	</center>
</body>
</html>